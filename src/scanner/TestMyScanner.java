package scanner;
import java.io.FileInputStream;
import java.io.InputStreamReader;

/**
 * Main to test MyScanner. Can call MyScanner with either a string or a .txt input file.
 * @author jackw
 *
 */
public class TestMyScanner
{

    public static void main( String[] args) 
    {
    	String input = args[0];				// Input argument
    	if (input.length() > 3) 			// If > 3, could be a .txt file.
    	{
    		if (input.substring(input.length() - 4).equals(".txt")) // If last 4 chars are .txt, is an input file
    		{
    			String filename = "scanner/"+args[0];		// Input comes from arg 1
    	        FileInputStream fis = null;
    	        try 
    			{
    	            fis = new FileInputStream( filename);		// Make sure the file exists
    	        } 
    			catch (Exception e ) { e.printStackTrace();}
    	        InputStreamReader isr = new InputStreamReader( fis);
    	        MyScanner scanner = new MyScanner( isr);
    	        Token aToken = null;
    	        do
    	        {
    	            try 
    				{
    	                aToken = scanner.nextToken();			// Try reading the next token
    	            }
    	            catch( Exception e) { e.printStackTrace();}
    	        } 
    			while( aToken != null);							// Repeat while token exists
    	        
        	} // End if
    		
    		else // Input > 4 and not txt file, input is then a string
    		{
    			MyScanner scanner = new MyScanner(input);
    			Token aToken = null;
    			do
    	        {
    	            try 
    				{
    	                aToken = scanner.nextToken();			// Try reading the next token
    	            }
    	            catch( Exception e) { e.printStackTrace();}
    	        } 
    			while( aToken != null);							// Repeat while token exists
    		}
    	}
    	else // Input < 4, therefore is a string
    	{
    		MyScanner scanner = new MyScanner(input);
			Token aToken = null;
			do
	        {
	            try 
				{
	                aToken = scanner.nextToken();			// Try reading the next token
	            }
	            catch( Exception e) { e.printStackTrace();}
	        } 
			while( aToken != null);							// Repeat while token exists
    	} // End else
    	
    } // End main 


}