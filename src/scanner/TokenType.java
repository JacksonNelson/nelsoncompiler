package scanner;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
//package simplescanner;

/**
 * TokenType is an enum that defines the possible types of tokens that can be scanned.
 * @author Jackson Nelson
 */
public enum TokenType {
    // Keywords
	PROGRAM, VAR, ARRAY, OF, INTEGER, REAL, FUNCTION, PROCEDURE, BEGIN, END, IF, THEN, ELSE, WHILE, DO, NOT, DIV, MOD, AND, OR, READ, WRITE, RETURN,
	// Symbols
	EQUALS, NOTEQUAL, LESSTHAN, LESSTHANEQUAL, GREATERTHANEQUAL, GREATERTHAN, PLUS, MINUS, MULTIPLY, DIVIDE, ASSIGNOP, LEFTCURLY, RIGHTCURLY, LEFTPARENTHESIS,
	RIGHTPARENTHESIS, DOT, LEFTBRACKET, RIGHTBRACKET, SEMICOLON, COLON, COMMA,
	// Other
	NUMBER, ID
}
