package scanner;

import static org.junit.Assert.*;
import java.io.IOException;
import org.junit.Test;
import scanner.Token;

public class NextTokenTest {

	/**
	 * Test of the nextToken() method, of class MyScanner.
	 * Tests if the nextToken method returns the proper tokentype.
	 */
	@Test
	public void testNextToken() 
	{
		System.out.println("Testing nextToken:");
		// Create a scanner
        MyScanner instance = new MyScanner("input");
        
        // Initialize the actual token to null
        Token actual = null;
		try 
		{
			// Try to assign actual as the nextToken
			actual = instance.nextToken();
		} 
		catch (IOException e) 
		{
			e.printStackTrace();
		}
		
		// Create a token with the expected output
        Token expected = new Token("input", TokenType.ID);
        
        // Compare the actual token to expected token. Prints message on failure.
        assertEquals("Tokens not equal.", expected.type, actual.type);
		
        // If we get here, tokens are equal.
		System.out.println("Expected: " + expected.lex + ", " + expected.type);
		System.out.println("Actual: " + actual.lex + ", " + actual.type);
		
		System.out.println("Tokens are equal.");
	}

}
