/**
 * This is a simple scanner created using the jflex tool.
 * Author: Jackson Nelson
 */

/* Declarations */

package scanner;
import java.util.HashMap;
import java.io.StringReader;

%%
%public
%class  MyScanner   /* Names the produced java file */
%function nextToken /* Renames the yylex() function */
%type   Token      /* Defines the return type of the scanning function */
%eofval{
  return null;
%eofval}

%{
		private String theSource = "";
		private int currentIndex = 0;
		private HashMap<String,TokenType> lookupTable = new HashMap<String,TokenType>();

		/**
		* This function initializes the lookup table with the keywords and symbols of the grammar
		*/
		public void initializeTable() 
		{
			// put stuff in the lookup table
			// KEYWORDS
			lookupTable.put("program", TokenType.PROGRAM);
			lookupTable.put("var", TokenType.VAR);
			lookupTable.put("array", TokenType.ARRAY);
			lookupTable.put("of", TokenType.OF);
			lookupTable.put("integer", TokenType.INTEGER);
			lookupTable.put("real", TokenType.REAL);
			lookupTable.put("function", TokenType.FUNCTION);
			lookupTable.put("procedure", TokenType.PROCEDURE);
			lookupTable.put("begin", TokenType.BEGIN);
			lookupTable.put("end", TokenType.END);
			lookupTable.put("if", TokenType.IF);
			lookupTable.put("then", TokenType.THEN);
			lookupTable.put("else", TokenType.ELSE);
			lookupTable.put("while", TokenType.WHILE);
			lookupTable.put("do", TokenType.DO);
			lookupTable.put("not", TokenType.NOT);
			lookupTable.put("div", TokenType.DIV);
			lookupTable.put("mod", TokenType.MOD);
			lookupTable.put("and", TokenType.AND);
			lookupTable.put("or", TokenType.OR);
			lookupTable.put("write", TokenType.WRITE);
			
			// SYMBOLS
			lookupTable.put("=", TokenType.EQUALS);
			lookupTable.put("<>", TokenType.NOTEQUAL);
			lookupTable.put("<", TokenType.LESSTHAN);
			lookupTable.put("<=", TokenType.LESSTHANEQUAL);
			lookupTable.put(">=", TokenType.GREATERTHANEQUAL);
			lookupTable.put(">", TokenType.GREATERTHAN);
			lookupTable.put("+", TokenType.PLUS);
			lookupTable.put("-", TokenType.MINUS);
			lookupTable.put("*", TokenType.MULTIPLY);
			lookupTable.put("/", TokenType.DIVIDE);
			lookupTable.put(":=", TokenType.ASSIGNOP);
			lookupTable.put("{", TokenType.LEFTCURLY);
			lookupTable.put("}", TokenType.RIGHTCURLY);
			lookupTable.put("(", TokenType.LEFTPARENTHESIS);
			lookupTable.put(")", TokenType.RIGHTPARENTHESIS);
			lookupTable.put(".", TokenType.DOT);
			lookupTable.put("[", TokenType.LEFTBRACKET);
			lookupTable.put("]", TokenType.RIGHTBRACKET);
			lookupTable.put(";", TokenType.SEMICOLON);
			lookupTable.put(":", TokenType.COLON);
			lookupTable.put(",", TokenType.COMMA);
			
		}
		
  /**
  * Creates a new scanner with a string input.
  *
  * @param   input  the string to read input from.
  */
  public MyScanner(String input) 
  {
	  StringReader in = new java.io.StringReader(input);
	  this.zzReader = in;
  }
		
		
%}


/* Patterns */

doubleSymbols     = (<= | <> | >= | :=)
other        	  = .
letter        	  = [A-Za-z]

digit		  	  = [0-9]
integer		  	  = {digit}{digit}*
optional_fraction = {integer}(\.){integer}*
optional_exponent = {integer}* (\.) ({integer})* (\e) (\+ | \-) ({integer})*
/* num 			  = {integer} | {optional_exponent} | {optional_fraction} */

id			  	  = {letter} ({letter} | {digit})*

whitespace    	  = [ \n\t\r]

%%
/* Lexical Rules */

{integer}		{
				/** Prints the num found */
				System.out.println("Found a num: " + yytext());
				return (new Token(yytext(), TokenType.INTEGER));
			}
			
			
{optional_fraction}		{
				/** Prints the num found */
				System.out.println("Found a num: " + yytext());
				return (new Token(yytext(), TokenType.REAL));
			}

{id}     {
             /** Print out the word that was found. Initializes the hashmap and searches for the token. If found, prints the keyword. Otherwise, prints it as ID. */
			 initializeTable();
			 if (lookupTable.containsKey(yytext()))
			 {
				System.out.println("Keyword found: " + yytext());
				TokenType keyword = lookupTable.get(yytext());
				return (new Token(yytext(), keyword));
			 }
			 else 
			 {
				System.out.println("ID found: " + yytext());
				return (new Token(yytext(), TokenType.ID));
			 }
             
            }
					
            
{whitespace}  {  /* Ignore Whitespace */ 
                 
              }

{other}    { 
			 /** Initializes the hashmap, searches for the element, and if found prints the symbol. Otherwise is an illegal char. */
			 initializeTable();
			 if (lookupTable.containsKey(yytext()))
			 {
				System.out.println("Symbol found: " + lookupTable.get(yytext()));
				TokenType symbol = lookupTable.get(yytext());
				return (new Token(yytext(), symbol));
			 }
			 else 
			 {
				System.out.println("Illegal char: '" + yytext() + "' found.");
			 }
             
           }
		   
{doubleSymbols}    
		{ 
			/** Initializes the hashmap, searches for the element, and if found prints the symbol. Otherwise is an illegal char. */
			
			 initializeTable();
			 if (lookupTable.containsKey(yytext()))
			 {
				System.out.println("Symbol found: " + lookupTable.get(yytext()));
				TokenType symbol = lookupTable.get(yytext());
				return (new Token(yytext(), symbol));
			 }
			 else 
			 {
				System.out.println("Illegal char: '" + yytext() + "' found.");
			 }
             
        }
		   