package scanner;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Defines a Token object, which is composed of a String lexeme and TokenType.
 * @author Jackson Nelson
 */
public class Token {
    public String lex;
    public TokenType type;
	
    public Token( String l, TokenType t) {
        this.lex = l;
        this.type = t;
    }
    
///////////////////////////////
//     Getters and Setters
///////////////////////////////

    public String getLexeme() 
    {
    	return this.lex;
    }
    public TokenType getType() 
    { 
    	return this.type;
    }
}
