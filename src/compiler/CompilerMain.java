/**
 * @author Jackson Nelson
 * Compiler main
 * This class acts as the main driver for the compiler.
 */
package compiler;
import parser.*;
import scanner.*;
import syntaxtree.ProgramNode;
import syntaxtree.StatementNode;
import syntaxtree.VariableNode;

import java.io.*;
import java.util.ArrayList;

import analysis.SemanticAnalyzer;
import codegen.CodeGeneration;

public class CompilerMain 
{
	public static void main(String[] args)
	{
		//System.out.println("Begin main");
		String filename = "";
		// Input file
		if (args.length > 0)
		{
			filename = args[0];
		}
		else
		{
			System.out.println("Args empty");
			return;
		}
		//System.out.println(filename);
		// Call recognizer on the file
		Parser p = new Parser(filename, true);
		// find the filetype chars
		int di = filename.indexOf(".");
		// Separate base from filetype
		String basename = filename.substring(0, di);
		
		//// Initialize files
		
		// Symbol Table file
		Writer outfile = null;
		// Syntax Tree file
		Writer outfile2 = null;
		// Assembly file
		Writer outfile3 = null;
		
		// Begin matching the program
		// Create indented toString
		ProgramNode pNode = p.program();
		String indentedToString = pNode.indentedToString(0);
		
		// Grab the symbol table
		String tableStrings = p.table.toString();
		
		
		SemanticAnalyzer sa = new SemanticAnalyzer(pNode, p.table);
		ArrayList<StatementNode> statements = pNode.getMain().getStatements();
		
		for(StatementNode state : statements)
		{
			sa.setTest1(sa.testDataTypeAssignment(state));
			sa.setTest2(sa.testVariableDeclaration(state, p.table));
			if (sa.getTest1() == false)
			{
				System.out.println("Type mismatch across statement");
			}
			if (sa.getTest2() == false)
			{
				System.out.println("Undeclared variable");
			}
		}
		
		if (sa.getTest1() && sa.getTest2())
		{
			sa.setGood(true);
		}
		// If analysis is good, write ASM to file
		if (sa.getGoodToGo())
		{	
			// Generate assembly from the syntax tree
			CodeGeneration gen = new CodeGeneration(p.progNode, p.table);
			String assembly = gen.writeCodeForRoot(p.progNode);
			
			try
			{
				// symbol table file
				outfile = new BufferedWriter(new FileWriter(basename + ".table"));
				// Write the table to file
				outfile.write(tableStrings);
				
				// toString file
				outfile2 = new BufferedWriter(new FileWriter(basename + ".indent"));
				// Write the table to file
				outfile2.write(indentedToString);
				
				// Assembly file
				outfile3 = new BufferedWriter(new FileWriter(basename + ".asm"));
				// Write the generated assembly
				outfile3.write(assembly);
			}
		
			catch (IOException e) 
			{
				e.printStackTrace();
			}
			
			try 
			{
				outfile.close();
			} 
			catch (IOException e) 
			{
				e.printStackTrace();
			}
			try 
			{
				outfile2.close();
			} 
			catch (IOException e) 
			{
				e.printStackTrace();
			}
			
			try
			{
				outfile3.close();
			}
			catch (IOException e)
			{
				e.printStackTrace();
			}
	
		}
	}
}
