package syntaxtree;

import java.util.ArrayList;

public class ParameterNode extends SyntaxTreeNode{
	ArrayList<VariableNode> vars = new ArrayList<VariableNode>();
	
	/**
	 * Add a variable to the function's parameter list
	 * @param var Variable to be added
	 */
	public void addVariable(VariableNode var)
	{
		vars.add(var);
	}
	
	/**
	 * Get the list of a function's parameters
	 * @return
	 */
	public ArrayList<VariableNode> getParameterList()
	{
		return vars;
	}
	
	
	@Override
	public String indentedToString(int level) {
		String answer = this.indentation( level);
        answer += "Parameters\n";
        for( VariableNode variable : vars) {
            answer += variable.indentedToString( level + 1);
        }
        return answer;
    }
		

}
