package syntaxtree;

import parser.DataType;

/**
 * General representation of any expression.
 * @author erik
 */
public abstract class ExpressionNode extends SyntaxTreeNode {
	DataType type;
	
	/**
	 * Set the data type of the expression node
	 * @param type
	 */
	public void setType(DataType type)
	{
		this.type = type;
	}
	
	
	/**
	 * Returns the data type of the expression node
	 * @return Type The type, integer or real, of the expression node
	 */
	public DataType getType()
	{
		return this.type;
	}
    
}
