package syntaxtree;

import org.junit.Test;
import static org.junit.Assert.*;

import scanner.TokenType;

public class TestSyntaxTree 
{
	/**
     * Test of indentedToString method, of classes ProgramNode, 
     * DeclarationsNode, SubProgramDeclarationsNode, CompoundStatementNode.
     */
    @Test
    public void testSyntaxTree() 
    {
		System.out.println("indentedToString for sample tree");
	    int level = 0;
	    ProgramNode root = new ProgramNode("sample");
	    
	    
	    /////////////////////////////////
	    //////// Set the declarations tree
	    /////////////////////////////////
	    DeclarationsNode variables = new DeclarationsNode();
	    VariableNode dollars = new VariableNode("Dollars");
	    VariableNode yen = new VariableNode("Yen");
	    VariableNode bitcoin = new VariableNode("Bitcoin");
	    variables.addVariable(dollars);
	    variables.addVariable(yen);
	    variables.addVariable(bitcoin);
	    
	    //////////////////////////////////////
	    //Set the subprogram declarations tree
	    //////////////////////////////////////
	    SubProgramDeclarationsNode functions = new SubProgramDeclarationsNode();
	    
	    
	    ///////////////////////////////////
	    ///Set the Compound Statement tree
	    //////////////////////////////////
	    CompoundStatementNode main = new CompoundStatementNode();
	    
	    // Assignment 1 subtree
	    AssignmentStatementNode assign1 = new AssignmentStatementNode();
	    ValueNode val1 = new ValueNode("1000000");
	    assign1.setLvalue(dollars);
	    assign1.setExpression(val1);
	    main.addStatement(assign1);
	    
	    // Assignment 2 subtree
	    AssignmentStatementNode assign2 = new AssignmentStatementNode();
	    assign2.setLvalue(yen);
	    OperationNode yenOp = new OperationNode(TokenType.MULTIPLY);
	    ValueNode val2 = new ValueNode("102");
	    yenOp.setLeft(dollars);
	    yenOp.setRight(val2);
	    assign2.setLvalue(yen);
	    assign2.setExpression(yenOp);
	    main.addStatement(assign2);
	    
	    // Assignment 3 subtree
	    AssignmentStatementNode assign3 = new AssignmentStatementNode();
	    assign3.setLvalue(bitcoin);
	    OperationNode bitOp = new OperationNode(TokenType.DIVIDE);
	    ValueNode val3 = new ValueNode("400");
	    bitOp.setLeft(dollars);
	    bitOp.setRight(val3);
	    assign3.setLvalue(bitcoin);
	    assign3.setExpression(bitOp);
	    main.addStatement(assign3);
	    
	    
	    root.setVariables(variables);
	    root.setFunctions(functions);
	    root.setMain(main);
	    
	    System.out.println( root.indentedToString(0));
	    try
	    {
	    	String expected = "Program: sample\n" + 
	    					  "|-- Declarations\n"+ 
	    					  "|-- --- Name: Dollars\n" +
	    					  "|-- --- Name: Yen\n" + 
	    					  "|-- --- Name: Bitcoin\n" + 
	    					  "|-- SubProgramDeclarations\n"+
	    					  "|-- CompoundStatement\n"+
	    					  "|-- --- Assignment\n"+
	    					  "|-- --- --- Name: Dollars\n"+
	    					  "|-- --- --- Value: 1000000\n"+
	    					  "|-- --- Assignment\n"+
	    					  "|-- --- --- Name: Yen\n"+
	    					  "|-- --- --- Operation: MULTIPLY\n"+
	    					  "|-- --- --- --- Name: Dollars\n" +
	    					  "|-- --- --- --- Value: 102\n" +
	    					  "|-- --- Assignment\n"+
	    					  "|-- --- --- Name: Bitcoin\n"+
	    					  "|-- --- --- Operation: DIVIDE\n"+
	    					  "|-- --- --- --- Name: Dollars\n"+
	    					  "|-- --- --- --- Value: 400\n";
	    	String actual = root.indentedToString(0);
	    	assertEquals(expected,actual);
	    }
	    catch(Exception e)
	    {
	    	fail(e.getMessage());
	    	System.out.println("ToString is not equal to Syntax Tree");
	    }
    }
}
