package syntaxtree;

public class SubProgramDeclarationNode extends SyntaxTreeNode
{
	private String head;
	//private SubProgramHeadNode head;
	private DeclarationsNode variables;
    private CompoundStatementNode main;
    private ParameterNode parameters;
    
    public SubProgramDeclarationNode( String aName) {
        this.head = aName;
    }

    public String getName()
    {
    	return head;
    }
    public DeclarationsNode getVariables() {
        return variables;
    }

    public void setVariables(DeclarationsNode variables) {
        this.variables = variables;
    }

    public CompoundStatementNode getMain() {
        return main;
    }

    public void setMain(CompoundStatementNode main) {
        this.main = main;
    }
    
    public void setParameters(ParameterNode param)
    {
    	this.parameters = param;
    }
    
    public ParameterNode getParameters() {
        return parameters;
    }

	/**
     * Creates a String representation of this subprogramDeclaration node and
     * its children.
     * @param level The tree level at which this node resides.
     * @return A String representing this node.
     */
	@Override
	public String indentedToString(int level) 
	{
		String answer = this.indentation( level);
        answer += "Function " + head + "\n";
        //answer += arguments.indentedToString( level +1);
        answer += variables.indentedToString( level + 1);
        answer += main.indentedToString( level + 1);
        return answer;
	}
	
}
