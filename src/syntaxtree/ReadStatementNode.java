
package syntaxtree;

/**
 * Represents a read statement in Pascal.
 * A read statement includes an ID.
 * @author Jackson Nelson
 */
public class ReadStatementNode extends StatementNode {
	private StatementNode state;
	private String id;
    
    public String getId()
    {
    	return id;
    }
    
    public void setId(String id)
    {
    	this.id = id;
    }
    
    /**
     * Creates a String representation of this node and its children.
     * @param level The tree level at which this node resides.
     * @return A String representing this node.
     */
    @Override
    public String indentedToString( int level) {
        String answer = this.indentation( level);
        answer += "Read\n";
        answer += this.state.indentedToString( level + 1);
        return answer;
    }

}
