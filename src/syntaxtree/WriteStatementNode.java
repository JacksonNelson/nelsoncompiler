
package syntaxtree;

/**
 * Represents a write statement in Pascal.
 * A write statement includes a boolean expression.
 * @author Jackson Nelson
 */
public class WriteStatementNode extends StatementNode {
    private ExpressionNode test;
    //private StatementNode doStatement;
    //private StatementNode elseStatement;

    public ExpressionNode getTest() {
        return test;
    }

    public void setTest(ExpressionNode test) {
        this.test = test;
    }
    
    public WriteStatementNode(ExpressionNode exp) 
    {
		this.test = exp;
	}

    //public StatementNode getDoStatement() {
    //    return doStatement;
    //}

    //public void setDoStatement(StatementNode doStatement) {
    //    this.doStatement = doStatement;
    //}

    //public StatementNode getElseStatement() {
    //    return elseStatement;
    //}

    //public void setElseStatement(StatementNode elseStatement) {
    //    this.elseStatement = elseStatement;
    //}
    
    /**
     * Creates a String representation of this node and its children.
     * @param level The tree level at which this node resides.
     * @return A String representing this node.
     */
    @Override
    public String indentedToString( int level) {
        String answer = this.indentation( level);
        answer += "Write\n";
        answer += this.test.indentedToString( level + 1);
        //answer += this.doStatement.indentedToString( level + 1);
        //answer += this.elseStatement.indentedToString( level + 1);
        return answer;
    }

}
