package codegen;

import java.util.ArrayList;

import parser.DataType;
import scanner.TokenType;
import syntaxtree.AssignmentStatementNode;
import syntaxtree.CompoundStatementNode;
import syntaxtree.DeclarationsNode;
import syntaxtree.ExpressionNode;
import syntaxtree.IfStatementNode;
import syntaxtree.OperationNode;
import syntaxtree.ProgramNode;
import syntaxtree.ReturnStatementNode;
import syntaxtree.StatementNode;
import syntaxtree.SubProgramDeclarationNode;
import syntaxtree.ValueNode;
import syntaxtree.VariableNode;
import syntaxtree.WhileStatementNode;
import syntaxtree.WriteStatementNode;
import parser.SymbolTable;

/**
 * This class will create MIPS assembly for a Pascal program
 * @author Jackson Nelson
 */
public class CodeGeneration {
    
    private int currentTRegister = 0;
    private int currentFRegister = 0;
    private ProgramNode root;
    private SymbolTable table;
    
    
    public CodeGeneration(ProgramNode program, SymbolTable st)
    {
    	this.root = program;
    	this.table = st;
    }
    
    /**
     * Starts the code from the root node by writing the outline of the
     * assembly code, and telling the root node to write its answer into $s0.
     *
     * @param root The root node of the program
     * @return A String of the assembly code.
     */
    public String writeCodeForRoot( ProgramNode root) 
    {
    	// Initialize code and write the first line
    	StringBuilder code = new StringBuilder();
        code.append( ".data\n");
    	
    	// For each declared variable, grab the name and put it in a list
    	DeclarationsNode dn = root.getVariables();
    	ArrayList<VariableNode> vars = dn.getVariables();
    	ArrayList<String> varNames = new ArrayList<String>();
    	for (VariableNode var : vars)
    	{
    		varNames.add(var.getName());
    	}
    	
    	// For each name in a list, write the MIPS for it
        for (String name : varNames)
        {
        	code.append(name + ":   .word   0\n");
        }
        
        // Write out last two lines of header
        code.append( ".text\n");
        code.append( "main:\n");
        
        // Move stack pointer and save return address
        code.append("addi $sp, $sp, -4\n");
        code.append("sw $ra, 0($sp)\n");
        
        // Write the main body of ASM
        String mainCode = writeCodeForMain(root.getMain());
        // Add main body onto existing code
        code.append(mainCode);
        code.append("\n");
        // Move the stack pointer back, jump back to return address
        code.append("lw $ra, 0($sp)\n");
        code.append("addi $sp, $sp, 4\n");
        code.append("jr $ra\n");
        
        return( code.toString());
    }
    
    /**
     * Writes the code for each statement grabbed from the CSN
     * @param main The CSN that has each statement
     * @return A String of ASM code
     */
    public String writeCodeForMain(CompoundStatementNode main)
    {
    	String code = "";
    	ArrayList<StatementNode> mainCode = main.getStatements();
    	for(StatementNode state : mainCode)
    	{
    		if (state != null)
    		{
    			code += writeCode(state);
    		}
    		
    	}
    	
    	return code;
    }
    
    
    /**
     * Writes the code for a subprogram declaration. Used for both functions and procedures
     * @param spdn The function or procedure
     */
    public String writeCodeForSubprogram(SubProgramDeclarationNode spdn)
    {
    	String code = spdn.getName() + ":\n";
    	code += "addi   $sp,   $sp,   -4\n";
    	code += "sw     $ra,   0($sp)\n";
    	
    	code += writeCodeForMain(spdn.getMain());
    	code += "lw    $ra,    0($sp)\n";
    	code += "addi   $sp,   $sp,    4";
    	code += "jr $ra";
    	
    	return code;
    }
    
    
    /**
     * Starts the code from the root node by writing the outline of the
     * assembly code, and telling the root node to write its answer into $s0.
     *
     * @param root The root node of the equation to be written
     * @return A String of the assembly code.
     */
    public String writeCodeForRoot( ExpressionNode root) 
    {
        StringBuilder code = new StringBuilder();
        code.append( ".data\n");
        code.append( "answer:   .word   0\n\n\n");
        code.append( ".text\n");
        code.append( "main:\n");
        
        String nodeCode = null;
        int tempTRegValue = this.currentTRegister;
        nodeCode = writeCode( root, "$s0");
        this.currentTRegister = tempTRegValue;
        code.append( nodeCode);
        code.append( "sw     $s0,   answer\n");
        code.append( "addi   $v0,   10\n");
        code.append( "syscall\n");
        return( code.toString());
    }
    
    /**
     * Writes code for the given node.
     * This generic write code takes any ExpressionNode, and then
     * recasts according to subclass type for dispatching.
     * @param node The node for which to write code.
     * @param reg The register in which to put the result.
     * @return 
     */
    public String writeCode( ExpressionNode node, String reg) {
        String nodeCode = null;
        if( node instanceof OperationNode) {
            nodeCode = writeCode( (OperationNode)node, reg);
        }
        else if( node instanceof ValueNode) {
            nodeCode = writeCode( (ValueNode)node, reg);
        }
        else if (node instanceof VariableNode)
        {
        	nodeCode = writeCode( (VariableNode)node, reg);
        }
        return( nodeCode);
    }
    
    /**
     * Writes code for an operations node.
     * The code is written by gathering the child nodes' answers into
     * a pair of registers, and then executing the op on those registers,
     * placing the result in the given result register.
     * @param opNode The operation node to perform.
     * @param resultRegister The register in which to put the result.
     * @return The code which executes this operation.
     */
    public String writeCode( OperationNode opNode, String resultRegister)
    {
        String code;
        String rightRegister = "";
        String leftRegister = "";
        ExpressionNode left = opNode.getLeft();
        if (left.getType() == DataType.REAL)
        {
        	leftRegister = "$f" + currentFRegister++;
        	if (! resultRegister.contains("$f"))
        	{
        		resultRegister = "$f" + resultRegister;
        	}
        	
        }
        else if (left.getType() == DataType.INTEGER)
        {
        	leftRegister = "$t" + currentTRegister++;
        	resultRegister = "$t" + resultRegister;
        }
        
        code = writeCode( left, leftRegister);
        
        
        ExpressionNode right = opNode.getRight();
        if (right.getType() == DataType.REAL)
        {
        	rightRegister = "$f" + currentFRegister++;
        }
        else if (right.getType() == DataType.INTEGER)
        {
        	rightRegister = "$t" + currentTRegister++;
        }
        
        code += writeCode( right, rightRegister);
        TokenType kindOfOp = opNode.getOperation();
        
        // ADDOPS
        if( kindOfOp == TokenType.PLUS)
        {
        	
            if (right.getType() == DataType.REAL)
            {
            	code += "add.s    " + resultRegister + ",   " + leftRegister +
                        ",   " + rightRegister + "\n";
            }
            else if (right.getType() == DataType.INTEGER)
            {
            	code += "add    " + resultRegister + ",   " + leftRegister +
                        ",   " + rightRegister + "\n";
            }

        }
        if( kindOfOp == TokenType.MINUS)
        {
            // sub resultregister, left, right 
           
            if (right.getType() == DataType.REAL)
            {
            	code += "sub.s    " + resultRegister + ",   " + leftRegister +
                        ",   " + rightRegister + "\n";
            }
            else if (right.getType() == DataType.INTEGER)
            {
            	code += "sub    " + resultRegister + ",   " + leftRegister +
                        ",   " + rightRegister + "\n";
            }
              
        }
        
        // MULOPS
        if( kindOfOp == TokenType.MULTIPLY)
        {
            if (right.getType() == DataType.REAL)
            {
            	code += "mul.s   " + resultRegister + ",   " + leftRegister + ",   " + rightRegister + "\n";
                
            }
            else if (right.getType() == DataType.INTEGER)
            {
            	code += "mult   " + leftRegister + ",   " + rightRegister + "\n";
                code += "mflo   " + resultRegister + "\n";
            }
            
            
        }
        if (kindOfOp == TokenType.DIVIDE || kindOfOp == TokenType.DIV)
        {

        	if (right.getType() == DataType.REAL)
            {
            	code += "div.s   " + resultRegister + ",   " + leftRegister + ",   " + rightRegister + "\n";
                
            }
            else if (right.getType() == DataType.INTEGER)
            {
            	code += "div " + leftRegister + ", " + rightRegister + "\n";
            	code += "mflo " + resultRegister + "\n";
            }
        	
        }
        if (kindOfOp == TokenType.MOD)
        {
        	code += "div " + leftRegister + ", " + rightRegister + "\n";
        	code += "mfhi " + resultRegister + "\n";
        }
        
        // RELOPS
        // Write for the inverse of each token type
        // For instance, with TT Equals, branch if they are not equal, otherwise continue
        if (kindOfOp == TokenType.EQUALS)
        {
        	code += "bne " + leftRegister + ", " + rightRegister + ", Else\n";
        }
        if (kindOfOp == TokenType.NOTEQUAL)
        {
        	code += "beq " + leftRegister + ", " + rightRegister + ", Else\n";
        }
        if (kindOfOp == TokenType.LESSTHAN)
        {
        	code += "bgt " + leftRegister + ", " + rightRegister + ", Else\n";
        }
        if (kindOfOp == TokenType.LESSTHANEQUAL)
        {
        	code += "bge " + leftRegister + ", " + rightRegister + ", Else\n";
        }
        if (kindOfOp == TokenType.GREATERTHANEQUAL)
        {
        	code += "ble " + leftRegister + ", " + rightRegister + ", Else\n";
        }
        if (kindOfOp == TokenType.GREATERTHAN)
        {
        	code += "blt " + leftRegister + ", " + rightRegister + ", Else\n";
        }
        
        this.currentTRegister -= 2;
        this.currentFRegister -= 2;
        return( code);
    }
    
    /**
     * Writes code for a value node.
     * The code is written by executing an add immediate with the value
     * into the destination register.
     * Writes code that looks like  addi $reg, $zero, value
     * @param valNode The node containing the value.
     * @param resultRegister The register in which to put the value.
     * @return The code which executes this value node.
     */
    public String writeCode( ValueNode valNode, String resultRegister)
    {
        String value = valNode.getAttribute();
        String code = "";
        if (value.contains("."))
        {
        	code = "li.s   " + resultRegister + ",   " + value + "\n";
        }
        else
        {
        	code = "addi   " + resultRegister + ",   $zero, " + value + "\n";
        }
        
        return( code);
    }
    
    
    
    /**
     * Writes code for a variable node.
     * @param valNode The node containing the value.
     * @param resultRegister The register in which to put the value.
     * @return The code which executes this value node.
     */
    public String writeCode( VariableNode var, String resultRegister)
    {
    	String code = "";
        String name = var.getName();
        if (var.getType() == DataType.INTEGER)
        {
        	code = "lw   " + resultRegister + ",   " + name + "\n";
        }
        else if (var.getType() == DataType.REAL)
        {
        	code = "lwc1   " + resultRegister + ",   " + name + "\n";
        }
        
        return( code);
    }
    
    
    
    
    /**
     * Writes code for the given node.
     * This generic write code takes any StatementNode, and then
     * recasts according to subclass type for dispatching.
     * @param node The node for which to write code.
     */
    public String writeCode( StatementNode node)
    {
    	String nodeCode = null;
        if( node instanceof AssignmentStatementNode) 
        {
            nodeCode = writeCode( (AssignmentStatementNode)node);
        }
        
        else if(node instanceof IfStatementNode)
        {
        	nodeCode = writeCode( (IfStatementNode)node);
        }
        else if (node instanceof ReturnStatementNode)
        {
        	nodeCode = writeCode( (ReturnStatementNode)node);
        }
        else if (node instanceof WhileStatementNode)
        {
        	nodeCode = writeCode( (WhileStatementNode)node);
        }
        else if (node instanceof WriteStatementNode)
        {
        	nodeCode = writeCode( (WriteStatementNode)node);
        }
        return( nodeCode);
    }
    
    
    /**
     * Write code for an assignment statement node
     * @param assignNode The node we are writing assembly for
     * @param resultRegister The register we are writing to
     * @return
     */
    public String writeCode( AssignmentStatementNode assignNode)
    {
    	String var = assignNode.getLvalue().getName();
    	String code = "";
        if (assignNode.getExpression() instanceof ValueNode)
        {
        	String value = assignNode.getExpression().toString();
        	if (assignNode.getExpression().getType() == DataType.INTEGER)
        	{
	        	code += "li $t" + currentTRegister + ", " + value + "\n";
	        	code += "sw $t" + currentTRegister + ", " + var + "\n";
	        	currentTRegister++;
        	}
        	else if (assignNode.getExpression().getType() == DataType.REAL)
        	{
        		code += "li.s $f" + currentFRegister + ", " + value + "\n";
	        	code += "sw $f" + currentFRegister + ", " + var + "\n";
	        	currentFRegister++;
        	}
        	else
        	{
        		System.out.println("DATATYPE ERROR");
        		//code += "DATATYPE ERROR\n";
        	}
        }
        else if (assignNode.getExpression() instanceof OperationNode)
        {
        	if (assignNode.getExpression().getType() == DataType.INTEGER)
        	{
        		String resultReg = String.valueOf(currentTRegister);
            	currentTRegister++;
            	
            	code += writeCode( (OperationNode)assignNode.getExpression(), resultReg);
        		
	        	code += "\n";
	        	code += "add $t" + currentTRegister + ", $t" + resultReg + ", $zero\n";
	        	code += "sw $t" + currentTRegister + ", " + var + "\n";
        	}
        	else if (assignNode.getExpression().getType() == DataType.REAL)
        	{
        		String resultReg = String.valueOf(currentFRegister);
            	//currentFRegister++;
            	
            	code += writeCode( (OperationNode)assignNode.getExpression(), resultReg);
        		
	        	code += "\n";
	        	//code += "add.s $f" + currentFRegister + ", $f" + resultReg + ", $zero\n";
	        	code += "sw $f" + currentFRegister + ", " + var + "\n";
        	}
        	else
        	{
        		code += "DATATYPE ERROR";
        	}
        	
        }
        //String code = "addi   " + resultRegister + ",   $zero, " + value + "\n";
        currentTRegister--;
        currentFRegister--;
        return( code);
    }
    
    
    /**
     * Write code method for return statement
     * @param returnNode The node writing asm for
     * @param resultRegister The result register
     * @return
     */
    public String writeCode( ReturnStatementNode returnNode)
    {
    	//if (returnNode.getTest() instanceof VariableNode)
    	//{
    	//	String var = returnNode.getTest().toString();
    		
    	//}
    	String code = "";
    	if (returnNode.getTest() instanceof ValueNode)
    	{
    		if(returnNode.getTest().getType() == DataType.INTEGER)
    		{
    			String val = returnNode.getTest().toString();
    			code += "li $v0, 5\n";
    			code += "li $a0, " + val + "\n";
    			code += "syscall\n";
    		}
    		else
    		{
    			String val = returnNode.getTest().toString();
    			code += "li $v0, 2\n";
    			code += "li $a0, " + val + "\n";
    			code += "syscall\n";
    		}
        	
    	}
    	
    	return code;
    }
    
    /**
     * Writes the assembly for an if statement
     * @param ifNode The given if-statement
     * @return A string of mips assembly
     */
    public String writeCode( IfStatementNode ifNode)
    {
    	String code = "";
    	
    	ExpressionNode test = ifNode.getTest();
    	// If we need to save a result, save it to current t reg. Write the test exp
    	code += writeCode(test, "$t" + currentTRegister++);
    	// Write the code for 'then' statement
    	code += writeCode(ifNode.getThenStatement());
    	// Jump past the else if we don't need it
    	code += "jal endIf" + "\n";
    	// Write the 'else' case
    	code += "Else:\n";
    	code += writeCode(ifNode.getElseStatement());
    	// End of the if statement
    	code += "endIf:" + "\n";
    	
    	currentTRegister--;
    	return code;
    }
    
    /**
     * Writes the assembly for a while statement
     * @param whileNode The given while node 
     * @return A string of mips assembly
     */
    public String writeCode (WhileStatementNode whileNode)
    {
    	// Loop point at top of a while statement
    	String code = "top:" + "\n";
    	// Write for the test
    	code += writeCode(whileNode.getTest(), "$t" + currentTRegister++);
    	code += writeCode(whileNode.getDoStatement());
    	// Jump back to top of while
    	code += "jal top" + "\n";
    	// Branch to 'Else' when test is false
    	code += "Else:\n";
    	
    	currentTRegister--;
    	return code;
    }
    
    
    public String writeCode(WriteStatementNode node) 
    {
		String code = "";
		
		if (node.getTest() instanceof OperationNode) {
			if (node.getTest().getType() == DataType.INTEGER) {
				code += writeCode(node.getTest(), "$s0");
				code += "li      $v0,   1\n";
				code += "move    $a0,   $s0\n";
				code += "syscall\n";
			}
		}
		else if (node.getTest() instanceof VariableNode) {
			code += writeCode(node.getTest(), "$s0");
			code += "li      $v0,   1\n";
			code += "move    $a0,   " + node.getTest() + "\n";
			code += "syscall\n";
		}
		
		return code;
	}
    
    
}
