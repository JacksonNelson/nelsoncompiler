/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package codegen;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

import parser.DataType;
import parser.Parser;
import syntaxtree.AssignmentStatementNode;
import syntaxtree.ExpressionNode;
import syntaxtree.OperationNode;
import syntaxtree.ProgramNode;
import syntaxtree.StatementNode;
import syntaxtree.SubProgramDeclarationNode;
import syntaxtree.ValueNode;

/**
 *
 * @author Jackson Nelson
 */
public class TestCodeGen {
    
    public TestCodeGen() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }

//    /**
//     * Test of writeCodeForRoot method, of class CodeGeneration.
//     */
//    @Test
//    public void testWriteCodeForExpression() {
//        System.out.println("writeCodeForExpression");
//        //Parser p = new Parser( "4 + (3*7)", false);
//        Parser p = new Parser( "foo := 3.7", false);
//        ExpressionNode root = p.expression();
//        
//        System.out.println("Test: \n \n \n");
//        System.out.println(root.indentedToString(1));
//        System.out.println("Test: \n \n \n");
//        CodeGeneration instance = new CodeGeneration(p.progNode, p.table);
//        String expResult = 
//                ".data\n" +
//			"answer:   .word   0\n" +
//			"\n" +
//			"\n" +
//			".text\n" +
//			"main:\n" +
//			"addi   $t0,   $zero, 4\n" +
//			"addi   $t2,   $zero, 3\n" +
//			"addi   $t3,   $zero, 7\n" +
//			"mult   $t2,   $t3\n" +
//			"mflo   $t1\n" + 
//			"add    $s0,   $t0,   $t1\n" +
//			"sw     $s0,   answer\n" +
//			"addi   $v0,   10\n" +
//			"syscall\n" +
//			"";
//        String result = instance.writeCodeForRoot(root);
//        System.out.println("Code is \n" + result);
//        assertEquals(expResult, result);
//    }

    
    
//    /**
//     * Test of writeCode for assignment, of class CodeGeneration.
//     */
//    @Test
//    public void testWriteCodeForAssign() 
//    {
//    	Parser p = new Parser("foo := 3", false);
//    	StatementNode assign = p.statement();
//    	((AssignmentStatementNode) assign).getExpression().setType(DataType.REAL);
//    	CodeGeneration instance = new CodeGeneration(p.progNode, p.table);
//    	String expected = 
//    			"li $t0, 3\n"
//    			+ "sw $t0, foo\n";
//    	
//    	String result = instance.writeCode(assign);
//    	System.out.println("Code is \n" + result);
//    	assertEquals(expected, result);
//    }
    
    
//    /**
//     * Test of writeCodeForRoot
//     */
//    @Test
//    public void testProgram()
//    {
//    	Parser p = new Parser("program foo; begin end .", false);
//    	ProgramNode prog = p.program();
//    	CodeGeneration gen = new CodeGeneration(p.progNode, p.table);
//    	String expected = 
//    			".data\n" +
//    			".text\n" +
//    			"main:\n" + 
//    			"addi $sp, $sp, -4\n" + 
//    			"sw $ra, 0($sp)\n" +
//    			"lw $ra, 0($sp)\n" +
//    			"addi $sp, $sp, 4\n" +
//    			"jr $ra\n";
//    	
//    	String actual = gen.writeCodeForRoot(prog);
//    	System.out.println("Code is\n" + actual);
//    	assertEquals(expected, actual);
//    }
    
    
    
    ///////////////////////////// Floating point tests //////////////////////////
    
//    /**
//     * Test of writeCode for assignment, of class CodeGeneration.
//     */
//    @Test
//    public void testWriteCodeForAssign() 
//    {
//    	Parser p = new Parser("foo := 3.0 + 3.1", false);
//    	StatementNode assign = p.statement();
//    	((AssignmentStatementNode) assign).getExpression().setType(DataType.REAL);
//    	CodeGeneration instance = new CodeGeneration(p.progNode, p.table);
//    	String expected = 
//    			"li.s   $f0,   3.0\n" +
//    			"li.s   $f1,   3.1\n" +
//    			"add.s    $f0,   $f0,   $f1\n" +
//    			"\n"+
//    			"sw $f0, foo\n";
//    	
//    	String result = instance.writeCode(assign);
//    	System.out.println("Code is \n" + result);
//    	assertEquals(expected, result);
//    }
//    
//    
//    
//    /**
//     * Test of writeCode for assignment, of class CodeGeneration.
//     */
//    @Test
//    public void testWriteCodeForAssign2() 
//    {
//    	Parser p = new Parser("foo := 3.0 * (3.1 + 3.2)", false);
//    	StatementNode assign = p.statement();
//    	((AssignmentStatementNode) assign).getExpression().setType(DataType.REAL);
//    	CodeGeneration instance = new CodeGeneration(p.progNode, p.table);
//    	String expected = 
//    			"li.s   $f0,   3.0\n" +
//    			"li.s   $f2,   3.1\n" +
//    			"li.s   $f3,   3.2\n" +
//    			"add.s    $f1,   $f2,   $f3\n" +
//    			"mul.s   $f0,   $f0,   $f1\n" +
//    			"\n"+
//    			"sw $f0, foo\n";
//    	
//    	String result = instance.writeCode(assign);
//    	System.out.println("Code is \n" + result);
//    	assertEquals(expected, result);
//    }
//    
//    
//    
//    
//    
//    /**
//     * Test of writeCode for assignment, of class CodeGeneration.
//     */
//    @Test
//    public void testWriteProgramWithFloats() 
//    {
//    	String test = "program foo;" +
//    				"var fee, fi, fo: real;"+
//    				"begin "+
//    					"fee := 4.2;"+
//    					"fi := 5.1;"+
//    					"fo := 3.0*(4.7 + 6.3);"+
//    					"end"+
//    					".";
//    	Parser p = new Parser(test, false);
//    	ProgramNode program = p.program();
//    	
//    	CodeGeneration instance = new CodeGeneration(p.progNode, p.table);
//    	String expected = 
//    			".data\n" +
//    			"fee:   .word   0\n" +
//    			"fi:   .word   0\n" +
//    			"fo:   .word   0\n" +
//    			
//    			".text\n"+
//    			"main:\n"+
//    			"addi $sp, $sp, -4\n"+
//    			"sw $ra, 0($sp)\n"+
//    			"li.s $f0, 4.2\n"+
//    			"sw $f0, fee\n"+
//    			"li.s $f0, 5.1\n"+
//    			"sw $f0, fi\n"+
//    			"li.s   $f0,   3.0\n"+
//    			"li.s   $f2,   4.7\n"+
//    			"li.s   $f3,   6.3\n"+
//    			"add.s    $f1,   $f2,   $f3\n"+
//    			"mul.s   $f0,   $f0,   $f1\n\n"+
//    			"sw $f0, fo\n\n"+
//    			"lw $ra, 0($sp)\n"+
//    			"addi $sp, $sp, 4\n"+
//    			"jr $ra\n";
//    	
//    	String result = instance.writeCodeForRoot(program);
//    	System.out.println("Code is \n" + result);
//    	assertEquals(expected, result);
//    }
    
    
    
    @Test
	public void testSubProgs()
	{
		String test = "function bar(foo: integer): integer; begin foo := 3 end .";
		
		Parser p = new Parser(test, false);
		CodeGeneration gen = new CodeGeneration(p.progNode, p.table);
		
		SubProgramDeclarationNode spdn = p.subprogramDeclaration();
		
		//String result = spdn.indentedToString(0);
		String result = gen.writeCodeForSubprogram(spdn);
		
		System.out.println(result);
	}
    
    
    
    
 }
