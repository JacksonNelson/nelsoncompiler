package analysis;

import java.util.ArrayList;

import parser.DataType;
import parser.Parser;
import parser.SymbolTable;
import syntaxtree.ProgramNode;
import syntaxtree.ReturnStatementNode;
import syntaxtree.StatementNode;
import syntaxtree.ValueNode;
import syntaxtree.VariableNode;
import syntaxtree.WhileStatementNode;
import syntaxtree.WriteStatementNode;
import syntaxtree.AssignmentStatementNode;
import syntaxtree.ExpressionNode;
import syntaxtree.IfStatementNode;
import syntaxtree.OperationNode;

public class SemanticAnalyzer 
{
	//Parser p = new Parser();
	private ProgramNode program;
	private SymbolTable symbol;
	
	private boolean goodToGo;
	private boolean test1;
	private boolean test2;
	
	
	/**
	 * Constructor for Semantic Analyzer
	 * @param prog Base program node
	 * @param table Global symbol table
	 */
	public SemanticAnalyzer(ProgramNode node, SymbolTable tab)
	{
		this.program = node;
		this.symbol = tab;
	}
	
	
//	/**
//	 * Constructor for Semantic Analyzer
//	 * @param prog Base program node
//	 * @param table Global symbol table
//	 */
//	public SemanticAnalyzer(ProgramNode prog, SymbolTable table)
//	{
//		this.program = prog;
//		this.symbol = table;
//		
//		// Our semantic analysis tests
//		//this.test1 = false;
//		//this.test2 = false;
//		
//		// Final check. Will be true if all tests are true
//		//this.goodToGo = false;
//		
//		ArrayList<StatementNode> statements = prog.getMain().getStatements();
//		System.out.println(statements.size());
//		for(StatementNode state : statements)
//		{
//			this.setTest1(testDataTypeAssignment(state));
//			this.setTest2(testVariableDeclaration(state, table));
//			
//			// This test still needs to be fixed
//			//boolean test3 = assignDataType(state);
//			
//			if (getTest1() == false)
//			{
//				System.out.println("Type mismatch across statement");
//			}
//			if (getTest2() == false)
//			{
//				System.out.println("Undeclared variable");
//			}
//		}
//		if (getTest1() && getTest2())
//		{
//			goodToGo = true;
//			System.out.println("Success!");
//		}
//		else
//		{
//			System.out.println("1: " + getTest1());
//			System.out.println("2: " + getTest2());
//			System.out.println("Fail!");
//		}
//	}
	
	
	/**
	 * Return if test1 succeeded
	 * @return
	 */
	public boolean getTest1()
	{
		return test1;
	}
	
	/**
	 * Return if test2 succeeded
	 * @return
	 */
	public boolean getTest2()
	{
		return test2;
	}
	
	public void setTest1(boolean test)
	{
		this.test1 = test;
	}
	
	public void setTest2(boolean test)
	{
		this.test2 = test;
	}
	
	public boolean getGoodToGo()
	{
		return this.goodToGo;
	}
	
	public void setGood(boolean bool)
	{
		this.goodToGo = bool;
	}
	
	
//	public void analyze(ProgramNode pNode, SymbolTable table)
//	{
//		SemanticAnalyzer sa = new SemanticAnalyzer(pNode, table);
//		ArrayList<StatementNode> statements = pNode.getMain().getStatements();
//		
//		for(StatementNode state : statements)
//		{
//			sa.setTest1(sa.testDataTypeAssignment(state));
//			sa.setTest2(sa.testVariableDeclaration(state, table));
//			if (sa.getTest1() == false)
//			{
//				System.out.println("Type mismatch across statement");
//			}
//			if (sa.getTest2() == false)
//			{
//				System.out.println("Undeclared variable");
//			}
//		}
//		
//		if (sa.getTest1() && sa.getTest2())
//		{
//			this.goodToGo = true;
//		}
//	}
	

	
	/**
	 * Function that ensures data types match across assignment
	 * @param state The statemen we are checking
	 * @return False if data types don't match, true otherwise
	 */
	public boolean testDataTypeAssignment(StatementNode state)
	{
		// Check for an assignment statement
		if (state instanceof AssignmentStatementNode)
		{
			// If left side is null, just set it to right side
			if (((AssignmentStatementNode) state).getLvalue().getType() == null)
			{
				((AssignmentStatementNode) state).getLvalue().setType(
						((AssignmentStatementNode) state).getExpression().getType());
			}
			
			// if left side type doesn't match right side type
			else if ( (((AssignmentStatementNode) state).getLvalue().getType())
					!= (((AssignmentStatementNode) state).getExpression().getType()) )
			{
				System.out.println("Statement: \n" + 
				((AssignmentStatementNode) state).indentedToString(0) + "\ndoes not match types.");
				return false;
			}
		}
		
		
		return true;
		
	}
	
	
	/**
	 * HELPER function to determine if a variable has been declared
	 * Tests against EXPRESSION
	 * @param exp The expression node we are checking
	 * @param st The symbol table to compare against
	 */
	public boolean testVariableDeclaration(ExpressionNode exp, SymbolTable st)
	{
		// Variable node is base case. Check if st contains the var
		if (exp instanceof VariableNode)
		{
			if (! (st.containsIdName(((VariableNode) exp).getName())))
			{
				System.out.println("Variable " + 
				((VariableNode) exp).getName() + " not declared.");
				return false;
			}
			else
			{
				return true;
			}
		}
		// If not a var, break the problem up until we find a variable node
		else if (exp instanceof OperationNode)
		{
			// Check both sides
			boolean one = testVariableDeclaration(((OperationNode) exp).getLeft(), st);
			boolean two = testVariableDeclaration(((OperationNode) exp).getRight(), st);
			// Return true if both sides are good
			return (one && two);
			
		}
		
		// If we find a value instead, we're fine
		else if (exp instanceof ValueNode)
		{
			return true;
		}
		
		// Default return true
		return true;
	}
	
	
	/**
	 * Recursive function to determine if a variable has been declared
	 * Uses helper function when testing expressions
	 * @param state The statement to check
	 */
	public boolean testVariableDeclaration(StatementNode state, SymbolTable table)
	{
		// Case 1: Assignment statement. Check var on left, expression on right
		if (state instanceof AssignmentStatementNode)
		{
			// Grab left side
			VariableNode var = ((AssignmentStatementNode) state).getLvalue();
			// Check if table doesn't contain var name
			if (! table.containsIdName(var.getName()))
			{
				System.out.println("Variable " + 
				var.getName() + " not declared.");
				return false;
			}
			
			// Grab right side
			ExpressionNode right = ((AssignmentStatementNode) state).getExpression();
			
			// Call the helper function on right side
			boolean testRight = testVariableDeclaration(right, table);
			
			// End case 1
			return testRight;
			
		}
		
		// Case 2: If statement. Check the test, check 'then' statement, check 'else' statement
		else if (state instanceof IfStatementNode)
		{
			// Grab the test expression
			ExpressionNode test = ((IfStatementNode) state).getTest();
			boolean checkTest = testVariableDeclaration(test,table);
			
			// If the test was bad, stop here
			if (checkTest == false)
			{
				return checkTest;
			}
			// Otherwise, keep going
			else
			{
				// Grab next part
				StatementNode thenStatement = ((IfStatementNode) state).getThenStatement();
				boolean checkThen = testVariableDeclaration(thenStatement, table);
				
				// If 'then' was bad, stop here
				if (checkThen == false)
				{
					return false;
				}
				// Otherwise, keep going
				else
				{
					StatementNode elseState = ((IfStatementNode) state).getElseStatement();
					boolean checkElse = testVariableDeclaration(elseState, table);
					
					// End case 2
					return checkElse;
				}
			}
		}
		
		
		// Case 3: While statement. Check 'test', then check 'do'
		else if (state instanceof WhileStatementNode)
		{
			// Grab the test exp
			ExpressionNode test = ((WhileStatementNode) state).getTest();
			boolean checkTest = testVariableDeclaration(test,table);
			
			// If test was bad, stop here
			if (checkTest == false)
			{
				return false;
			}
			// Otherwise, keep going
			else
			{
				// Grab the 'do'
				StatementNode doState = ((WhileStatementNode) state).getDoStatement();
				boolean checkDo = testVariableDeclaration(doState, table);
				
				// End case 3
				return checkDo;
			}
			
		}
		
		// Case 4: Write statement
		else if (state instanceof WriteStatementNode)
		{
			// Grab the test
			ExpressionNode testExp = ((WriteStatementNode) state).getTest();
			boolean checkTest = testVariableDeclaration(testExp, table);
			
			// End case 4
			return checkTest;
		}
		
		// Case 5: Return statement
		else if (state instanceof ReturnStatementNode)
		{
			ExpressionNode testExp = ((ReturnStatementNode) state).getTest();
			boolean checkTest = testVariableDeclaration(testExp, table);
			
			// End case 5
			return checkTest;
		}
		
		// If we get here, return true?
		return true;
	}
	
	
	/**
	 * Function to assign data types to each expression
	 * @param exp The expression to be assigned a type
	 */
	public boolean assignDataType(ExpressionNode exp)
	{
		
		if (exp instanceof OperationNode)
		{
			// Right side is also an operation
			if (((OperationNode) exp).getRight() instanceof OperationNode)
			{
				// Recursive call
				assignDataType(((OperationNode) exp).getRight());
				
				// Grab that type
				DataType right = ((OperationNode) exp).getRight().getType();
				
				// If the left side hasn't been declared yet, set it to right side
				// We don't want to override if left != right
				if (((OperationNode) exp).getLeft().getType() == null)
				{
					((OperationNode) exp).getLeft().setType(right);
					exp.setType(right);
					return true;
				}
				
			}
			
			
			else if (((OperationNode) exp).getRight() instanceof ValueNode)
			{
				// Right side is int, set it to be an INTEGER
				// If left is null, set it as INTEGER too
				if (findType(((OperationNode) exp).getRight()) == DataType.INTEGER)
				{
					((OperationNode) exp).getRight().setType(DataType.INTEGER);
					if (((OperationNode) exp).getLeft().getType() == null)
					{
						((OperationNode) exp).getLeft().setType(DataType.INTEGER);
						exp.setType(DataType.INTEGER);
					}
					
					return true;
				}
				
				else if (findType(((OperationNode) exp).getRight()) == DataType.REAL)
				{
					// Right is REAL, set it as REAL
					// If left is null, set REAL too
					((OperationNode) exp).getRight().setType(DataType.REAL);
					if (((OperationNode) exp).getLeft().getType() == null)
					{
						((OperationNode) exp).getLeft().setType(DataType.REAL);
						exp.setType(DataType.REAL);
					}
					
					// Left != null and Left != right
					// Assignment failed, return false
					else if ( (((OperationNode) exp).getLeft().getType() != null)
							&& (((OperationNode) exp).getLeft().getType() != 
							((OperationNode) exp).getRight().getType()) )
					{
						System.out.println("Mismatched types in expression: \n" + exp.indentedToString(0));
						return false;
					}
					
					
					return true;
				}
				
				DataType right = ((OperationNode) exp).getRight().getType();
				
				// Don't allow mismatching types in expression
				if ( (((OperationNode) exp).getLeft().getType() != null)
						&& (((OperationNode) exp).getLeft().getType() != right) )
				{
					System.out.println("Mismatched types in expression: \n" + exp.indentedToString(0));
					return false;
				}
				
			}
			
			else if (((OperationNode) exp).getRight() instanceof VariableNode)
			{
				// If we have two vars, and left is null, set left to right
				if (((OperationNode) exp).getLeft().getType() == null)
				{
					((OperationNode) exp).getLeft().setType(((OperationNode) exp).getRight().getType());
					exp.setType(((OperationNode) exp).getRight().getType());
				}
			}
			
		}
		
		// If we see a value, just set to REAL or INTEGER
		else if (exp instanceof ValueNode)
		{
			if (findType(exp) == DataType.REAL)
			{
				exp.setType(DataType.REAL);
			}
			else if (findType(exp) == DataType.INTEGER)
			{
				exp.setType(DataType.INTEGER);
			}
			return true;
		}
		
		//else if (exp instanceof VariableNode)
		//{
		//	//if (exp.g)
		//}
				
		return false;
		
	}
	
	/**
	 * Function to find data type of an expression.
	 * Checks if string contains 'e' or '.'
	 * @param exp The expression to be checked
	 * @return The DataType that is found
	 */
	public DataType findType(ExpressionNode exp)
	{
		if (exp.toString().contains(".") || exp.toString().contains("e"))
		{
			return DataType.REAL;
		}
		else
		{
			return DataType.INTEGER;
		}
	}
	
}
