	package analysis;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Test;

import parser.DataType;
import parser.Parser;
import parser.SymbolTable;
import scanner.TokenType;
import syntaxtree.AssignmentStatementNode;
import syntaxtree.ExpressionNode;
import syntaxtree.OperationNode;
import syntaxtree.StatementNode;
import syntaxtree.ValueNode;
import syntaxtree.VariableNode;
import analysis.SemanticAnalyzer;

public class TestSemanticAnalyzer {

//	/**
//	 * Negative test for variable declaration. Foo is NOT declared.
//	 */
//	@Test
//	public void testUndeclared() 
//	{
//		SemanticAnalyzer sa = new SemanticAnalyzer();
//		SymbolTable st = new SymbolTable();
//		VariableNode foo = new VariableNode("foo");
//		ValueNode five = new ValueNode("5");
//		
//		OperationNode exp1 = new OperationNode(TokenType.PLUS);
//		exp1.setLeft(foo);
//		exp1.setRight(five);
//		
//		
//		
//		// Var is not declared: if it returns true, method doesn't work.
//		if (sa.testVariableDeclaration(exp1, st) == true)
//		{
//			fail("Var not declared");
//		}
//			
//		
//	}
//	
//	/**
//	 * Test for checking a var declaration. Foo is declared
//	 */
//	@Test
//	public void testDeclared() 
//	{
//		SemanticAnalyzer sa = new SemanticAnalyzer();
//		SymbolTable st = new SymbolTable();
//		VariableNode foo = new VariableNode("foo");
//		ValueNode five = new ValueNode("5");
//		st.addVariable("foo", false);
//		
//		OperationNode exp1 = new OperationNode(TokenType.PLUS);
//		exp1.setLeft(foo);
//		exp1.setRight(five);
//		
//		// Var is declared: if returns false, method doesn't work
//		if (sa.testVariableDeclaration(exp1, st) == false)
//		{
//			fail("Var is declared");
//		}
//			
//		
//	}
//	
//	/**
//	* Test for automatic data type assignment. 
//	* foo + 5
//	*/
//	@Test
//	public void testDataType() 
//	{
//		SemanticAnalyzer sa = new SemanticAnalyzer();
//		SymbolTable st = new SymbolTable();
//		VariableNode foo = new VariableNode("foo");
//		ValueNode five = new ValueNode("5");
//		st.addVariable("foo", false);
//		
//		OperationNode exp1 = new OperationNode(TokenType.PLUS);
//		exp1.setLeft(foo);
//		exp1.setRight(five);
//		
//		// Expected type
//		DataType expected = DataType.INTEGER;
//		
//		// Should assign INTEGER
//		sa.assignDataType(exp1);
//		DataType actual = exp1.getType();
//		System.out.println(exp1.getType());
//		
//		
//		try
//		{
//			// If data type is either null or real, method doesn't work
//			assertEquals(expected,actual);
//			System.out.println("Assign success!");
//		}
//		catch(AssertionError e)
//		{
//			fail("Data type should be integer");
//		}
//		
//		
//	}
//	
//	/**
//	 * Test automatic type assignment. foo + bar + 5
//	 */
//	@Test
//	public void testDataType2() 
//	{
//		SemanticAnalyzer sa = new SemanticAnalyzer();
//		SymbolTable st = new SymbolTable();
//		VariableNode foo = new VariableNode("foo");
//		VariableNode bar=  new VariableNode("bar");
//		ValueNode five = new ValueNode("5");
//		//st.addVariable("foo", false);
//		
//		OperationNode exp1 = new OperationNode(TokenType.PLUS);
//		OperationNode exp2 = new OperationNode(TokenType.PLUS);
//		
//		exp2.setLeft(bar);
//		exp2.setRight(five);
//		
//		exp1.setLeft(foo);
//		exp1.setRight(exp2);
//		
//		// Expected type
//		DataType expected = DataType.INTEGER;
//		
//		// Should assign INTEGER
//		sa.assignDataType(exp1);
//		sa.assignDataType(exp2);
//		
//		DataType actual = exp1.getType();
//		DataType actual2 = exp2.getType();
//		System.out.println(actual);
//		
//		
//		try
//		{
//			// If data type is either null or real, method doesn't work
//			assertEquals(expected,actual);
//			System.out.println("Assign success!");
//		}
//		catch(AssertionError e)
//		{
//			fail("Data type should be integer");
//		}
//		
//		
//	}
//	
//	/**
//	 * Negative test of type assignment. foo + bar + 5.0
//	 */
//	@Test
//	public void testDataType3() 
//	{
//		SemanticAnalyzer sa = new SemanticAnalyzer();
//		SymbolTable st = new SymbolTable();
//		VariableNode foo = new VariableNode("foo");
//		VariableNode bar=  new VariableNode("bar");
//		ValueNode five = new ValueNode("5.0");
//		//st.addVariable("foo", false);
//		
//		OperationNode exp1 = new OperationNode(TokenType.PLUS);
//		OperationNode exp2 = new OperationNode(TokenType.PLUS);
//		
//		exp2.setLeft(bar);
//		exp2.setRight(five);
//		
//		exp1.setLeft(foo);
//		exp1.setRight(exp2);
//		
//		// Compare to an incorrect type
//		DataType wrongType = DataType.INTEGER;
//		
//		// Should assign REAL
//		sa.assignDataType(exp1);
//		sa.assignDataType(exp2);
//		
//		DataType actual = exp1.getType();
//		//DataType actual2 = exp2.getType();
//		System.out.println(actual);
//		
//		
//		try
//		{
//			// If data type is either null or INTEGER, method doesn't work
//			assertEquals(wrongType,actual);
//			fail("Data type should be REAL");
//			
//		}
//		catch(AssertionError e)
//		{
//			System.out.println("Assign success!");
//		}
//		
//		
//	}
//	
//	
//	/**
//	 * Negative test of type assignment. foo + bar + 5.0.
//	 * Bar is assigned INTEGER, attempt to add 5.0
//	 */
//	@Test
//	public void testDataType4() 
//	{
//		SemanticAnalyzer sa = new SemanticAnalyzer();
//		SymbolTable st = new SymbolTable();
//		VariableNode foo = new VariableNode("foo");
//		VariableNode bar=  new VariableNode("bar");
//		ValueNode five = new ValueNode("5.0");
//		//st.addVariable("foo", false);
//		bar.setType(DataType.INTEGER);
//		
//		OperationNode exp1 = new OperationNode(TokenType.PLUS);
//		OperationNode exp2 = new OperationNode(TokenType.PLUS);
//		
//		exp2.setLeft(bar);
//		exp2.setRight(five);
//		
//		exp1.setLeft(foo);
//		exp1.setRight(exp2);
//		
//		System.out.println("Left: " + exp2.getLeft().getType());
//		System.out.println("Right: " + exp2.getRight().getType());
//		
//		// Compare to an incorrect type
//		DataType wrongType = DataType.INTEGER;
//		
//		// Should assign REAL
//		sa.assignDataType(exp1);
//		sa.assignDataType(exp2);
//		
//		System.out.println("Left2: " + exp2.getLeft().getType());
//		System.out.println("Right2: " + exp2.getRight().getType());
//		
//		DataType actual = exp1.getType();
//		//DataType actual2 = exp2.getType();
//		System.out.println(actual);
//		
//		
//		try
//		{
//			// If data type is either null or INTEGER, method doesn't work
//			assertEquals(wrongType,actual);
//			fail("Data type should be REAL");
//			
//		}
//		catch(AssertionError e)
//		{
//			System.out.println("Assign success!");
//		}
//		
//		
//	}
//	
//	/**
//	 * Test for checking types across assignment statements.
//	 * foo := 5
//	 */
//	@Test
//	public void testAssign() 
//	{
//		SemanticAnalyzer sa = new SemanticAnalyzer();
//		AssignmentStatementNode sn = new AssignmentStatementNode();
//		VariableNode foo = new VariableNode("foo");
//		ValueNode five = new ValueNode("5");
//		sn.setLvalue(foo);
//		sn.setExpression(five);
//		sa.assignDataType(foo);
//		sa.assignDataType(five);
//		
//		//System.out.println(foo.getType());
//		//System.out.println(five.getType());
//		
//		boolean one = sa.testDataTypeAssignment(sn);
//		boolean two = true;
//		
//		try
//		{
//			assertEquals(one,two);
//			System.out.println("Assignment success");
//		}
//		
//		catch(Exception e)
//		{
//			fail("Assignment is correct: should not fail");
//		}
//		//System.out.println(foo.getType());
//		//System.out.println(one);
//		
//	}
//	
//	/**
//	 * Negative test for checking types across assignment.
//	 * Foo is integer, attempt foo := 5.0 
//	 */
//	@Test
//	public void testAssign2() 
//	{
//		SemanticAnalyzer sa = new SemanticAnalyzer();
//		AssignmentStatementNode sn = new AssignmentStatementNode();
//		VariableNode foo = new VariableNode("foo");
//		ValueNode five = new ValueNode("5.0");
//		sn.setLvalue(foo);
//		sn.setExpression(five);
//		
//		foo.setType(DataType.INTEGER);
//		sa.assignDataType(five);
//		
//		
//		boolean one = sa.testDataTypeAssignment(sn);
//		boolean two = true;
//		
//		try
//		{
//			assertEquals(one,two);
//			fail("Assignment is incorrect");
//			
//		}
//		
//		catch(AssertionError e)
//		{
//			System.out.println("Assignment success");
//		}
//		
//	}
	
	
	
	@Test
	public void testAssign2() 
	{
		String test = "program foo;" +
				"var fee, fi, fo, fum: integer;" + 
					"begin " + 
					  "fee := 4;"+
					  "fi := 5;"+
					  "fo := 3 * fee + fi;"+
					  "if fo < 13"+
					    "then "+
					      "fo := 13.3"+
					    "else "+
					      "fo := 26"+
					  ";"+
					  "write( fo)"+
					" end"+
					".";
		Parser p = new Parser(test, false);
		//String indentedToString = p.program().indentedToString(0);
		p.program();
		//SemanticAnalyzer sa = new SemanticAnalyzer(p.progNode, p.table);
		SemanticAnalyzer sa = new SemanticAnalyzer(p.progNode, p.table);
		ArrayList<StatementNode> statements = p.progNode.getMain().getStatements();
		boolean test1;
		boolean test2;
		System.out.println(statements.size());
		for(StatementNode state : statements)
		{
			
			test1 = sa.testDataTypeAssignment(state);
			
			test2 = sa.testVariableDeclaration(state, p.table);
			
			
			// This test still needs to be fixed
			//boolean test3 = assignDataType(state);
		}
		//System.out.println(indentedToString);
		
	}
}



