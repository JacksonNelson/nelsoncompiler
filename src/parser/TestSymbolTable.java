package parser;

import static org.junit.Assert.*;

import org.junit.Test;

import scanner.TokenType;

public class TestSymbolTable 
{
	
	SymbolTable table = new SymbolTable();

	@Test
	public void testAddVar() 
	{
		System.out.println("Begin add foo");
		table.addVariable("foo", true);
		
		if (table.containsIdName("foo") == true)
		{
			System.out.println("Success add foo!");
		}
		else
		{
			fail("Table does not contain foo");
		}
		
		System.out.println("Begin check foo");
		
		if (table.isVariableName("foo") == true)
		{
			System.out.println("Foo is Var");
		}
		else
		{
			fail("Foo is not var");
		}
		System.out.println(table.toString());
		System.out.println("--------------------");
	}
	
	@Test
	public void testAddFunction() 
	{
		System.out.println("Begin add doSomething");
		table.addFunction("doSomething");
		
		if (table.containsIdName("doSomething") == true)
		{
			System.out.println("Success add doSomething!");
		}
		else
		{
			fail("Table does not contain doSomething");
		}
		
		System.out.println("Begin check doSomething");
		
		if (table.isFunctionName("doSomething") == true)
		{
			System.out.println("doSomething is function");
		}
		else
		{
			fail("doSomething is not function");
		}
		
		System.out.println(table.toString());
		System.out.println("------------------");
		
	}
	
	@Test
	public void testAddArray() 
	{
		System.out.println("Begin add array");
		table.addArray("array");
		
		if (table.containsIdName("array") == true)
		{
			System.out.println("Success add array!");
		}
		else
		{
			fail("Table does not contain array");
		}
		
		System.out.println("Begin check array");
		
		if (table.isArrayName("array") == true)
		{
			System.out.println("array is Array");
		}
		else
		{
			fail("array is not Array");
		}
		System.out.println(table.toString());
		System.out.println("------------------");
		
	}
	
	@Test
	public void testAddProgram() 
	{
		System.out.println("Begin add prog");
		table.addProgram("prog");
		
		if (table.containsIdName("prog") == true)
		{
			System.out.println("Success add prog!");
		}
		else
		{
			fail("Table does not contain prog");
		}
		
		System.out.println("Begin check prog");
		
		if (table.isProgramName("prog") == true)
		{
			System.out.println("prog is program");
		}
		else
		{
			fail("prog is not program");
		}
		System.out.println(table.toString());
		System.out.println("------------------");
		
	}
	
	@Test
	public void testContainsId() 
	{
		System.out.println("Begin check foo");
		table.addVariable("foo", true);
		
		if (table.containsIdName("foo") == true)
		{
			System.out.println("Success check foo!");
		}
		else
		{
			fail("Table does not contain array");
		}
		
		System.out.println(table.toString());
		
		System.out.println("------------------");
		
	}
	
	

}
