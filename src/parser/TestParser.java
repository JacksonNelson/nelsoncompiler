package parser;

import static org.junit.Assert.*;

import org.junit.ComparisonFailure;
import org.junit.Test;

import scanner.TokenType;
import syntaxtree.*;

public class TestParser 
{

//	/**
//     * Test of indentedToString method of class factor
//     */
//	@Test
//	public void testFactor1() 
//	{
//		System.out.println("indentedToString for factor");
//	    //int level = 0;
//	    VariableNode factor = new VariableNode("foo");
//	    System.out.println(factor.indentedToString(0));
//	    
//	    try
//	    {
//	    	String expected = "Name: foo\n";
//	    	String actual = factor.indentedToString(0);
//	    	assertEquals(expected,actual);
//	    	System.out.println("factor 1 success");
//	    	System.out.println("------------------");
//	    }
//	    
//	    catch(Exception e)
//	    {
//	    	fail(e.getMessage());
//	    	System.out.println("ToString is not equal to Syntax Tree");
//	    	System.out.println("------------------");
//	    }
//		
//	}
//	
//	/**
//     * Test of indentedToString method of class factor.
//     * Negative test
//     */
//	@Test
//	public void testFactor2() 
//	{
//		System.out.println("indentedToString for factor 2");
//	    //int level = 0;
//	    VariableNode factor = new VariableNode("foo");
//	    System.out.println(factor.indentedToString(0));
//	    
//	    try
//	    {
//	    	String expected = "Name: bar\n";
//	    	String actual = factor.indentedToString(0);
//	    	assertEquals(expected,actual);
//	    	fail("Strings should not be equal");
//	    	System.out.println("------------------");
//	    }
//	    catch(ComparisonFailure e)
//	    {
//	    	System.out.println("factor 2 success");
//	    	System.out.println("------------------");
//	    }
//		
//	}
//	
//	/**
//     * Test of indentedToString method of class simple_expression.
//     */
//	@Test
//	public void testSimpleExpression1() 
//	{
//		System.out.println("indentedToString for simple_expression 1");
//	    //int level = 0;
//	    OperationNode root = new OperationNode(TokenType.PLUS);
//	    ValueNode five = new ValueNode("5");
//	    ValueNode six= new ValueNode("6");
//	    root.setLeft(five);
//	    root.setRight(six);
//	    System.out.println(root.indentedToString(0));
//	    
//	    try
//	    {
//	    	String expected = "Operation: PLUS\n"+
//					  "|-- Value: 5\n" +
//					  "|-- Value: 6\n";
//	    	String actual = root.indentedToString(0);
//	    	assertEquals(expected,actual);
//	    	System.out.println("factor 1 success");
//	    	System.out.println("------------------");
//	    }
//	    catch(Exception e)
//	    {
//	    	fail(e.getMessage());
//	    	System.out.println("ToString is not equal to Syntax Tree");
//	    	System.out.println("------------------");
//	    }
//		
//	}
//	
//	/**
//	 * Test of indentedToString method for class simple_expression
//	 */
//	@Test
//	public void testSimpleExpression2() 
//	{
//		System.out.println("indentedToString for simple_expression 2");
//	    //int level = 0;
//	    OperationNode root = new OperationNode(TokenType.PLUS);
//	    ValueNode five = new ValueNode("5");
//	    ValueNode six= new ValueNode("6");
//	    ValueNode seven = new ValueNode("7");
//	    
//	    OperationNode sub = new OperationNode(TokenType.PLUS);
//	    sub.setLeft(five);
//	    sub.setRight(six);
//	    
//	    root.setLeft(sub);
//	    root.setRight(seven);
//	    System.out.println(root.indentedToString(0));
//	    
//	    try
//	    {
//	    	String expected = "Operation: PLUS\n"+
//	    						"|-- Operation: PLUS\n" +
//					  			"|-- --- Value: 5\n" +
//					  			"|-- --- Value: 6\n" +
//					  			"|-- Value: 7\n";
//	    	String actual = root.indentedToString(0);
//	    	assertEquals(expected,actual);
//	    	System.out.println("simple_expression 2 success");
//	    	System.out.println("------------------");
//	    }
//	    catch(Exception e)
//	    {
//	    	fail(e.getMessage());
//	    	System.out.println("ToString is not equal to Syntax Tree");
//	    	System.out.println("------------------");
//	    }
//		
//	}
//	
//	/**
//	 * Test of indentedToString method for class simple_expression
//	 * Negative test
//	 */
//	@Test
//	public void testSimpleExpression3() 
//	{
//		System.out.println("indentedToString for simple_expression 3");
//	    //int level = 0;
//	    OperationNode root = new OperationNode(TokenType.PLUS);
//	    ValueNode five = new ValueNode("5");
//	    ValueNode six= new ValueNode("6");
//	    ValueNode seven = new ValueNode("7");
//	    
//	    OperationNode sub = new OperationNode(TokenType.PLUS);
//	    sub.setLeft(five);
//	    sub.setRight(six);
//	    
//	    root.setLeft(sub);
//	    root.setRight(seven);
//	    System.out.println(root.indentedToString(0));
//	    
//	    try
//	    {
//	    	String expected = "Operation: PLUS\n"+
//	    						"|-- Operation: PLUS\n" +
//					  			"|-- --- Value: 6\n" +
//					  			"|-- --- Value: 7\n" +
//					  			"|-- Value: 5\n";
//	    	String actual = root.indentedToString(0);
//	    	assertEquals(expected,actual);
//	    	fail("Strings should not be equal");
//	    	System.out.println("------------------");
//	    	
//	    }
//	    catch(ComparisonFailure e)
//	    {
//	    	System.out.println("simple_expression 3 success");
//	    	System.out.println("------------------");
//	    }
//		
//	}
//	
//	@Test
//	public void testSimpleExpression4()
//	{
//		System.out.println("indentedToString for simple_expression 4");
//	    //int level = 0;
//		String input = "5 * 6";
//		Parser p = new Parser(input, false);
//		ExpressionNode root = p.simpleExpression();
//	    //OperationNode root = new OperationNode(TokenType.MULTIPLY);
//	    //ValueNode five = new ValueNode("5");
//	    //ValueNode six= new ValueNode("6");
//	    //root.setLeft(five);
//	    //root.setRight(six);
//	    System.out.println(root.indentedToString(0));
//	    
//	    try
//	    {
//	    	String expected = "Operation: MULTIPLY\n"+
//					  "|-- Value: 5\n" +
//					  "|-- Value: 6\n";
//	    	String actual = root.indentedToString(0);
//	    	assertEquals(expected,actual);
//	    	System.out.println("simple_expression 4 success");
//	    	System.out.println("------------------");
//	    }
//	    catch(Exception e)
//	    {
//	    	fail(e.getMessage());
//	    	System.out.println("ToString4 is not equal to Syntax Tree");
//	    	System.out.println("------------------");
//	    }
//	}
//	
//	/**
//	 * Test of indentedToString method for class simple_expression
//	 */
//	@Test
//	public void testStatement1() 
//	{
//		System.out.println("indentedToString for statement 1");
//	    //int level = 0;
//		AssignmentStatementNode root = new AssignmentStatementNode();
//	    VariableNode var = new VariableNode("Foo");
//	    ValueNode five = new ValueNode("5");
//	    
//	    root.setLvalue(var);
//	    root.setExpression(five);
//	    
//	    System.out.println(root.indentedToString(0));
//	    
//	    try
//	    {
//	    	String expected = "Assignment\n"+
//					  			"|-- Name: Foo\n" +
//					  			"|-- Value: 5\n";
//	    	String actual = root.indentedToString(0);
//	    	assertEquals(expected,actual);
//	    	System.out.println("simple_expression 2 success");
//	    	System.out.println("------------------");
//	    }
//	    catch(Exception e)
//	    {
//	    	fail(e.getMessage());
//	    	System.out.println("ToString is not equal to Syntax Tree");
//	    	System.out.println("------------------");
//	    }
//		
//	}
//	
//	/**
//	 * Test of indentedToString method for class simple_expression
//	 */
//	@Test
//	public void testStatement2() 
//	{
//		System.out.println("indentedToString for statement 2");
//	    //int level = 0;
//		AssignmentStatementNode root = new AssignmentStatementNode();
//		OperationNode sub = new OperationNode(TokenType.PLUS);
//	    VariableNode foo = new VariableNode("Foo");
//	    VariableNode bar = new VariableNode("Bar");
//	    ValueNode five = new ValueNode("5");
//	    
//	    sub.setLeft(bar);
//	    sub.setRight(five);
//	    root.setLvalue(foo);
//	    root.setExpression(sub);
//	    
//	    System.out.println(root.indentedToString(0));
//	    
//	    try
//	    {
//	    	String expected = "Assignment\n"+
//					  			"|-- Name: Foo\n" +
//					  			"|-- Operation: PLUS\n" +
//					  			"|-- --- Name: Bar\n"+
//					  			"|-- --- Value: 5\n";
//	    	String actual = root.indentedToString(0);
//	    	assertEquals(expected,actual);
//	    	System.out.println("simple_expression 2 success");
//	    	System.out.println("------------------");
//	    }
//	    catch(Exception e)
//	    {
//	    	fail(e.getMessage());
//	    	System.out.println("ToString is not equal to Syntax Tree");
//	    	System.out.println("------------------");
//	    }
//		
//	}
//	
//	/**
//	 * Test of indentedToString method for class simple_expression
//	 */
//	@Test
//	public void testSpd1() 
//	{
//		System.out.println("indentedToString for subprogram declarations 1");
//	    //int level = 0;
//		SubProgramDeclarationsNode root = new SubProgramDeclarationsNode();
//		//OperationNode sub = new OperationNode(TokenType.PLUS);
//	    //VariableNode foo = new VariableNode("Foo");
//	    VariableNode bar = new VariableNode("Bar");
//	    //ValueNode five = new ValueNode("5");
//	    SubProgramDeclarationNode sub = new SubProgramDeclarationNode("Spd1");
//	    DeclarationsNode dec = new DeclarationsNode();
//	    dec.addVariable(bar);
//	    sub.setVariables(dec);
//	    
//	    CompoundStatementNode csn = new CompoundStatementNode();
//	    sub.setMain(csn);
//	    
//	    root.addSubProgramDeclaration(sub);
//	    System.out.println(root.indentedToString(0));
//	    
//	    try
//	    {
//	    	String expected = "SubProgramDeclarations\n" +
//	    						"|-- Function Spd1\n"+
//	    						"|-- --- Declarations\n" +
//					  			"|-- --- --- Name: Bar\n"+
//	    						"|-- --- CompoundStatement\n";
//	    	String actual = root.indentedToString(0);
//	    	assertEquals(expected,actual);
//	    	System.out.println("simple_expression 2 success");
//	    	System.out.println("------------------");
//	    }
//	    catch(Exception e)
//	    {
//	    	fail(e.getMessage());
//	    	System.out.println("ToString is not equal to Syntax Tree");
//	    	System.out.println("------------------");
//	    }
//		
//	}
//	
//	/**
//	 * Test of indentedToString method for class simple_expression
//	 */
//	@Test
//	public void testDeclarations() 
//	{
//		System.out.println("indentedToString for declarations 1");
//	    //int level = 0;
//		DeclarationsNode root = new DeclarationsNode();
//		//OperationNode sub = new OperationNode(TokenType.PLUS);
//	    VariableNode foo = new VariableNode("Foo");
//	    VariableNode bar = new VariableNode("Bar");
//	    //ValueNode five = new ValueNode("5");
//	    root.addVariable(bar);
//	    root.addVariable(foo);
//	    
//	    System.out.println(root.indentedToString(0));
//	    
//	    try
//	    {
//	    	String expected = "Declarations\n" +
//	    						"|-- Name: Bar\n"+
//	    						"|-- Name: Foo\n";
//	    	String actual = root.indentedToString(0);
//	    	assertEquals(expected,actual);
//	    	System.out.println("simple_expression 2 success");
//	    	System.out.println("------------------");
//	    }
//	    catch(Exception e)
//	    {
//	    	fail(e.getMessage());
//	    	System.out.println("ToString is not equal to Syntax Tree");
//	    	System.out.println("------------------");
//	    }
//		
//	}
//	
//	/**
//	 * Test of indentedToString method for class simple_expression
//	 */
//	@Test
//	public void testProgram() 
//	{
//		System.out.println("indentedToString for program 1");
//	    
//		ProgramNode root = new ProgramNode("Root");
//		
//		DeclarationsNode variables = new DeclarationsNode();
//	    VariableNode dollars = new VariableNode("Dollars");
//	    variables.addVariable(dollars);
//	    
//	    SubProgramDeclarationsNode functions = new SubProgramDeclarationsNode();
//	    
//	    CompoundStatementNode main = new CompoundStatementNode();
//	    
//	    AssignmentStatementNode assign1 = new AssignmentStatementNode();
//	    ValueNode val1 = new ValueNode("1000000");
//	    assign1.setLvalue(dollars);
//	    assign1.setExpression(val1);
//	    main.addStatement(assign1);
//	    
//	    
//	    root.setVariables(variables);
//	    root.setFunctions(functions);
//	    root.setMain(main);
//	    
//	    System.out.println(root.indentedToString(0));
//	    
//	    try
//	    {
//	    	String expected = "Program: Root\n" +
//	    						"|-- Declarations\n"+
//	    						"|-- --- Name: Dollars\n"+
//	    						"|-- SubProgramDeclarations\n"+
//		    					"|-- CompoundStatement\n"+
//		    					"|-- --- Assignment\n"+
//		    					"|-- --- --- Name: Dollars\n"+
//		    					"|-- --- --- Value: 1000000\n";
//	    	String actual = root.indentedToString(0);
//	    	assertEquals(expected,actual);
//	    	System.out.println("simple_expression 2 success");
//	    	System.out.println("------------------");
//	    }
//	    catch(Exception e)
//	    {
//	    	fail(e.getMessage());
//	    	System.out.println("ToString is not equal to Syntax Tree");
//	    	System.out.println("------------------");
//	    }
//		
//	}
	
	
	@Test
	public void testSubProgs()
	{
		String test = "function bar(foo: integer): integer; begin foo := 3 end .";
		
		Parser p = new Parser(test, false);
		p.subprogramDeclaration();
	}

}
