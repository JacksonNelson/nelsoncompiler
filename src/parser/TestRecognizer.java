/**
 * @author Jackson Nelson
 * TestRecognizer
 * This class uses jUnit to perform unit tests for the recognizer parsing several input strings
 */
package parser;

import static org.junit.Assert.*;

import org.junit.Test;

public class TestRecognizer {
 
	/**
	 * Test 1 - Standard type for 'Happy path'. Success is if the try statement completes.
	 * Tests standard_type
	 */
	@Test
	public void testStandardType() 
	{
		String happy = "2.0";
		Recognizer r = new Recognizer(happy, false);
		System.out.println("happy begin");
		
		try
		{
			r.standardType();
			System.out.println("happy success!");
			System.out.println("------------------");
		}
		catch (Exception e)
		{
			fail(e.getMessage());
		}
	}
	
	/**
	 * Test 2 - program with 'happy path'. Test is success if try completes.
	 * Tests program, declarations, subprogram declarations, compound statement.
	 */
	@Test
	public void testProgram() 
	{
		String happy = "program foo ; begin end .";
		Recognizer r = new Recognizer(happy, false);
		System.out.println("happy Begin!");
		
		try
		{
			r.program();
			System.out.println("happy success!");
			System.out.println("------------------");
			
		}
		catch (Exception e)
		{
			
			fail(e.getMessage());
			
		}
	}
	
	/**
	 * Test 3 = program with 'sad path'. Success is if program throws an exception.
	 * Tests program, declarations, subprogram declarations, compound statement.
	 * 
	 */
	@Test
	public void testProgram2()
	{
		// sad missing a semicolon
		String sad = "program foo begin end .";
		Recognizer r2 = new Recognizer(sad, false);
		
		try
		{
			r2.program();
			fail("Failure - parser should not accept this string.");
		}
		catch (Exception e)
		{
			System.out.println("happy success!");
			System.out.println("------------------");
		}
	}
	
	/**
	 * Test 4 - Simple expression with 'happy path'. Success is if try completes. 
 	 * Tests simple_expression, term, factor, term_part, simple_part.
	 */
	@Test
	public void testSimpleExpression()
	{
		String happy = "3+4*5";
		Recognizer r = new Recognizer(happy, false);
		
		try
		{
			r.simpleExpression();
			System.out.println("happy success!");
			System.out.println("------------------");
		}
		catch (Exception e)
		{
			fail(e.getMessage());
		}
		
		// Sad has misplaced equals sign. Success is if test throws an exception.
		// Tests simple_expression, term, factor, term_part, simple_part.
		String sad = "fox + = 3";
		Recognizer r2 = new Recognizer(sad, false);
		
		try
		{
			r2.simpleExpression();
			fail("Failure - parser should not accept this string.");
		}
		catch (Exception e)
		{
			System.out.println("sad success!");
			System.out.println("------------------");
		}
		
	}
	
	/**
	 * Test 5 - Statement with 'happy path'. Success is if try completes.
	 * Tests statement, variable, expression, simple_expression, term, factor, term_part, simple_part.
	 */
	@Test
	public void testStatement()
	{
		String happy = "if foo = 4 then foo := 3 else foo := 4;";
		Recognizer r = new Recognizer(happy, false);
		
		try
		{
			r.statement();
			System.out.println("happy success!");
			System.out.println("------------------");
		}
		catch (Exception e)
		{
			fail(e.getMessage());
		}
	}
	
	/**
	 * Test 6 - Statement with 'sad path'. Success is if try throws an exception.
	 * Tests statement, variable, expression, simple_expression, term, factor, term_part, simple_part.
	 */
	@Test
	public void testStatement2()
	{
		String happy = "foo *= 3";
		Recognizer r = new Recognizer(happy, false);
		
		try
		{
			r.statement();
			fail("Failure - parser should not recognize this string.");
			
		}
		catch (Exception e)
		{
			System.out.println("happy success!");
			System.out.println("------------------");
		}
	}

}
