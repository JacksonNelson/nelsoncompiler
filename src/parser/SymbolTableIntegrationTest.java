package parser;

import static org.junit.Assert.*;

import org.junit.Test;

public class SymbolTableIntegrationTest {

	/**
	 * Test 1 - program with 'happy path'. Test is success if try completes.
	 * Tests if recognizer can parse the string and add 'foo' to symbol table.
	 */
	@Test
	public void testProgram() 
	{
		String happy = "program foo ; begin end .";
		Recognizer r = new Recognizer(happy, false);
		System.out.println("happy Begin!");
		r.table.addProgram("foo");
		try
		{
			r.program();
			String expected = "Symbol 	Kind\n~~~~~~~~~~~~~~~~~\n" + 
					"foo\tPROGRAM\n";
			String actual = r.table.toString();
			assertEquals(expected,actual);
			System.out.println("------------------");			
		}
		catch (Exception e)
		{
			
			fail(e.getMessage());
			
		}
	}
	
	/**
	 * Test 2 - Statement with 'happy path'. Success is if try completes.
	 * Tests statement with variable assignment
	 */
	@Test
	public void testStatement()
	{
		String happy = "if foo = 4 then foo := 3 else foo := 4;";
		Recognizer r = new Recognizer(happy, false);
		r.table.addVariable("foo", true);
		try
		{
			r.statement();
			String expected = "Symbol 	Kind\n~~~~~~~~~~~~~~~~~\n" + 
					"foo\tVARIABLE\n";
			String actual = r.table.toString();
			assertEquals(expected,actual);
			//System.out.println("happy success!");
			System.out.println("------------------");
		}
		catch (Exception e)
		{
			fail(e.getMessage());
		}
	}
	
	/**
	 * Test 3 - Statement with 'happy path'. Success is if try completes.
	 * Tests program, statement, tests between a procedure call with variable
	 * assignment. Tests the toString method.
	 */
	@Test
	public void testProgram2()
	{
		String happy = "program foo ; var bar : integer; procedure pro; begin bar := 4 end ; begin end .";
		Recognizer r = new Recognizer(happy, false);
		
		try
		{
			r.program();
			String expected = "Symbol 	Kind\n~~~~~~~~~~~~~~~~~\n" + 
			"bar\tVARIABLE\n" + "foo\tPROGRAM\n" + "pro\tPROCEDURE\n";
			String actual = r.table.toString();
			//System.out.println(r.table.toString());
			assertEquals(expected,actual);
			System.out.println("------------------");
		}
		catch (Exception e)
		{
			fail(e.getMessage());
		}
		
	}
	
	/**
	 * Test 4 - Statement with 'happy path'. Success is if try completes.
	 * Tests statement with variable assignment
	 */
	@Test
	public void testStatement2()
	{
		String happy = "bar := 7";
		Recognizer r = new Recognizer(happy, false);
		r.table.addVariable("bar", true);
		
		try
		{
			r.statement();
			String expected = "Symbol 	Kind\n~~~~~~~~~~~~~~~~~\n" + 
					"bar\tVARIABLE\n";
			String actual = r.table.toString();
			assertEquals(expected,actual);
			//System.out.println("happy success!");
			System.out.println("------------------");
		}
		catch (Exception e)
		{
			fail(e.getMessage());
		}
	}
	
	/**
	 * Test 5 - Statement with sad path - foo should be a procedure, but is
	 * added as a variable. Should catch an exception, fail if it doesn't
	 */
	@Test
	public void testStatement3()
	{
		Recognizer r = new Recognizer("foo(3)", false);
		r.table.addVariable("foo", false);
		
		try
		{
			r.statement();
			fail("Foo should be a procedure, not var");
		}
		catch(Exception e)
		{
			System.out.println("sad statement success!");
			System.out.println("------------------");
		}
	}
	
	/**
	 * Test 6 - Program with sad path - pro should be a procedure, but is
	 * used like a variable by trying to assign it a value. Should catch an 
	 * exception, fail if it doesn't
	 */
	@Test
	public void testProgram3()
	{
		String sadProgram = "program foo ; var bar : integer; procedure pro; begin"
				+ " bar := 4; pro := 4 end ; begin end .";
		Recognizer r = new Recognizer(sadProgram, false);
		
		try
		{
			r.program();
			fail("Pro is a procedure, cannot have an assignop.");
		}
		catch (Exception e)
		{
			
			System.out.println("sad program success!");
			System.out.println("------------------");
		}
		//System.out.println(r.table.toString());
	}
	
	/**
	 * Test 7 - Program with sad path - fun should be a function, but is
	 * used like a variable by trying to assign it a value. Should catch an 
	 * exception, fail if it doesn't
	 */
	@Test
	public void testProgram4()
	{
		String sadProgram2 = "program foo ; var bar : integer; function fun: integer; "
				+ "begin bar := 4; fun := 5 end ; begin end .";
		Recognizer r = new Recognizer(sadProgram2, false);
		
		try
		{
			r.program();
			fail("Fun is a function, cannot have an assignop.");
		}
		catch (Exception e)
		{
			
			System.out.println("sad program2 success!");
			System.out.println("------------------");
		}
		//System.out.println(r.table.toString());
	}

}
