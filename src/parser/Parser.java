/**
 * @author Jackson Nelson
 * Recognizer.java
 * This class acts as the recognizer for a recursive descent parser.
 */
package parser;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringReader;
import java.util.ArrayList;

import scanner.*;
import syntaxtree.*;

public class Parser {
	
	///////////////////////////////
	//       Instance Variables
	///////////////////////////////
	private Token lookahead;
	private MyScanner scanner;
	public SymbolTable table = new SymbolTable();
	
	public ProgramNode progNode;
	
	
	
	///////////////////////////////
	//       Constructors
	///////////////////////////////
	
	public Parser( String text, boolean isFilename) 
	{
		if( isFilename) 
		{
			FileInputStream fis = null;
			try 
			{
				fis = new FileInputStream(text);
			} 
			catch (FileNotFoundException ex) 
			{
				error( "No file");
			}
			InputStreamReader isr = new InputStreamReader( fis);
			scanner = new MyScanner( isr);
			
		}
		else 
		{
			scanner = new MyScanner( new StringReader( text));
		}
		try 
		{
			lookahead = scanner.nextToken();
		} 
		catch (IOException ex) 
		{
			error( "Scan error");
		}
		
	}
	
	
	///////////////////////////////
	//       Methods
	///////////////////////////////
	
	/**
	 * Rules for program non-terminal symbol in grammar.
	 * @return prog A node representing the program keyword and its children
	 */
	public ProgramNode program()
	{
		match(TokenType.PROGRAM);
		String id = lookahead.lex;
		match(TokenType.ID);
		this.table.addProgram(id);
		//ProgramNode prog = new ProgramNode(lookahead.lex);
		this.progNode = new ProgramNode(id);
		match(TokenType.SEMICOLON);
		DeclarationsNode variables = declarations();
		progNode.setVariables(variables);
		SubProgramDeclarationsNode sub = subprogramDeclarations();
		progNode.setFunctions(sub);
		CompoundStatementNode main = compoundStatement();
		progNode.setMain(main);
		match(TokenType.DOT);
		return progNode;
		
	}
	
	/**
	 *  Rules for identifier_list non-terminal symbol in grammar
	 *  @return answer The a list containing all ids
	 */
	public ArrayList<String> identifierList()
	{
		ArrayList<String> answer= new ArrayList<String>();
		String name = lookahead.lex;
		answer.add(name);
		match(TokenType.ID);
		//System.out.println("Test: " + name);
		table.addVariable(name, false);
		
		if (lookahead.getType() == TokenType.COMMA)
		{
			match(TokenType.COMMA);
			//name = lookahead.lex;
			answer.addAll(identifierList());
			//match(TokenType.ID);
			//table.addVariable(name, false);
			//answer.addAll(identifierList());
		}
		return answer;
	}
	
	/**
	 *  Rules for declarations non-terminal symbol in grammar
	 *  @return dn A node representing declared vars
	 */
	public DeclarationsNode declarations()
	{
		DeclarationsNode dn = new DeclarationsNode();
		
		if (lookahead.getType() == TokenType.VAR)
		{
			match(TokenType.VAR);
			ArrayList<String> names = identifierList();
			for (String s: names)
			{
				dn.addVariable(new VariableNode(s));
			}
			match(TokenType.COLON);
			DataType type = type();
			
			ArrayList<VariableNode> varList = dn.getVariables();
			for (VariableNode var: varList)
			{
				var.setType(type);
			}
			match(TokenType.SEMICOLON);
			//declarations();
		}
		else
		{
			// Do nothing
		}
		return dn;
	}
	
	/**
	 * Rules for type non-terminal symbol in grammar
	 */
	public DataType type()
	{
		DataType type = null;
		switch(lookahead.getType())
		{
			case ARRAY:
				match(TokenType.ARRAY);
				match(TokenType.LEFTBRACKET);
				match(TokenType.NUMBER);
				match(TokenType.COLON);
				match(TokenType.NUMBER);
				match(TokenType.RIGHTBRACKET);
				match(TokenType.OF);
				type = standardType();
				break;
			default:
				type = standardType();
				break;
		}
		return type;
		
	}
	
	/**
	 * Rules for standard_type non-terminal symbol in grammar
	 */
	public DataType standardType()
	{
		DataType type = null;
		if (lookahead.getType() == TokenType.INTEGER)
		{
			match(TokenType.INTEGER);
			type = DataType.INTEGER;
		}
		else if (lookahead.getType() == TokenType.REAL)
		{
			match(TokenType.REAL);
			type = DataType.REAL;
		}
		
		return type;
	}
	
	/**
	 * Rules for subprogram_declarations non-terminal symbol in grammar
	 * @return withAnS A node representing all SP declarations
	 */
	public SubProgramDeclarationsNode subprogramDeclarations()
	{
		SubProgramDeclarationsNode withAnS = new SubProgramDeclarationsNode();
		
		if ((lookahead.getType() == TokenType.FUNCTION) || (lookahead.getType() == TokenType.PROCEDURE))
		{
			SubProgramDeclarationNode spd = subprogramDeclaration();
			withAnS.addSubProgramDeclaration(spd);
			match(TokenType.SEMICOLON);
			subprogramDeclarations();
			return withAnS;
		}
		else
		{
			// Do nothing
			return withAnS;
		}
	}
	
	/**
	 * Rules for subprogram_declaration non-terminal symbol in grammar
	 * @return spdn A node representing a single SP Declaration
	 */
	public SubProgramDeclarationNode subprogramDeclaration()
	{
		SubProgramDeclarationNode spdn = subprogramHead();
		//subprogramHead();
		DeclarationsNode subProgVars = declarations();
		spdn.setVariables(subProgVars);
		
		CompoundStatementNode main = compoundStatement();
		spdn.setMain(main);
		return spdn;
	}
	
	/**
	 * Rules for subprogram_head non-terminal symbol in grammar
	 * @return spdn A node representing the SP head
	 */
	public SubProgramDeclarationNode subprogramHead()
	{
		
		if(lookahead.getType() == TokenType.FUNCTION)
		{
			match(TokenType.FUNCTION);
			
			String id = lookahead.lex;
			table.addFunction(id);
			
			match(TokenType.ID);
			SubProgramDeclarationNode spdn = new SubProgramDeclarationNode(id);
			
			ArrayList<String> varNames = arguments();
			ParameterNode parameters = new ParameterNode();
			
			for (String name: varNames)
			{
				parameters.addVariable(new VariableNode(name));
			}
			
			spdn.setParameters(parameters);
			
			match(TokenType.COLON);
			standardType();
			
			match(TokenType.SEMICOLON);
			
			return spdn;
		}
			
		else if (lookahead.getType() == TokenType.PROCEDURE)
		{
			match(TokenType.PROCEDURE);
			
			String id = lookahead.lex;
			table.addProcedure(id);
			
			match(TokenType.ID);
			SubProgramDeclarationNode spdn = new SubProgramDeclarationNode(id);
			ArrayList<String> args = arguments();
			ParameterNode params = new ParameterNode();
			
			for (String name: args)
			{
				params.addVariable(new VariableNode(name));
			}
			
			spdn.setParameters(params);
			match(TokenType.SEMICOLON);
			return spdn;
		}
			
		else
		{
			error("Expected Function or Procedure but found "+ lookahead.getType());
			return null;
		}
			
	}
	
	/**
	 * Rules for arguments non-terminal symbol in grammar
	 * @return argList A list representing the args of a SPD
	 */
	public ArrayList<String> arguments()
	{
		if (lookahead.getType() == TokenType.LEFTPARENTHESIS)
		{
			match(TokenType.LEFTPARENTHESIS);
			ArrayList<String> argList = parameterList();
			match(TokenType.RIGHTPARENTHESIS);
			return argList;
		}
		else
		{
			// Do nothing
			return null;
		}
		
	}
	
	/**
	 * Rules for parameter_list non-terminal symbol in grammar
	 * @return paramList A list of all params of the arguments
	 */
	public ArrayList<String> parameterList()
	{
		ArrayList<String> paramList = identifierList();
		match(TokenType.COLON);
		type();
		if (lookahead.getType() == TokenType.SEMICOLON)
		{
			match(TokenType.SEMICOLON);
			parameterList();
		}
		else
		{
			// Do nothing
			//return null;
		}
		return paramList;
	}
	
	/**
	 * Rules for compound_statement non-terminal symbol in grammar
	 * @return csn A node representing the main body of a program
	 */
	public CompoundStatementNode compoundStatement()
	{
		match(TokenType.BEGIN);
		CompoundStatementNode csn = new CompoundStatementNode();
		//csn.addStatement(state);
		ArrayList<StatementNode> states = optionalStatements();
		for (StatementNode state: states)
		{
			if (! (state == null))
			{
				csn.addStatement(state);
			}
		}
		match(TokenType.END);
		return csn;
	}
	
	/**
	 * Rules for optional_statements non-terminal symbol in grammar
	 * @return stateList A list of optional statements
	 */
	public ArrayList<StatementNode> optionalStatements()
	{
		if (isStatement(lookahead))
		{
			ArrayList<StatementNode> stateList = statementList();
			return stateList;
		}
		else
		{
			// Do nothing
			return null;
		}
	}
	
	/**
	 * Rules for statement_list non-terminal symbol in grammar
	 * @return stateList A list of additional statements
	 */
	public ArrayList<StatementNode> statementList()
	{
		ArrayList<StatementNode> stateList = new ArrayList<StatementNode>();
		stateList.add(statement());
		if (lookahead.getType() == TokenType.SEMICOLON)
		{
			match(TokenType.SEMICOLON);
			stateList.addAll(statementList());
		}
		
		return stateList;
	}
	
	/**
	 * Rules for statement non-terminal symbol in grammar
	 * @return StatementNode A node representing a statement
	 */
	public StatementNode statement()
	{
		if (lookahead.getType() == TokenType.IF)
		{
			// Initial node
			IfStatementNode ifState = new IfStatementNode();
			match(TokenType.IF);
			// Add first expression
			ifState.setTest(expression());
			match(TokenType.THEN);
			// Then statement
			ifState.setThenStatement(statement());
			match(TokenType.ELSE);
			// Else statement
			ifState.setElseStatement(statement());
			// Return node
			return ifState;
		}
		else if (lookahead.getType() == TokenType.WHILE)
		{
			// Initial node
			WhileStatementNode whileState = new WhileStatementNode();
			match(TokenType.WHILE);
			// Add test expression
			whileState.setTest(expression());
			match(TokenType.DO);
			// Add do statement
			whileState.setDoStatement(statement());
			// Return node
			return whileState;
		}
		else if (lookahead.getType() == TokenType.READ)
		{
			match(TokenType.READ);
			match(TokenType.LEFTPARENTHESIS);
			//ReadStatementNode readState = new ReadStatementNode();
			match(TokenType.ID);
			match(TokenType.RIGHTPARENTHESIS);
			// return readState;
		}
		else if (lookahead.getType() == TokenType.WRITE)
		{
			match(TokenType.WRITE);
			WriteStatementNode writeState = new WriteStatementNode(null);
			match(TokenType.LEFTPARENTHESIS);
			writeState.setTest(expression());
			match(TokenType.RIGHTPARENTHESIS);
			return writeState;
		}
		else if (lookahead.getType() == TokenType.RETURN)
		{
			match(TokenType.RETURN);
			ReturnStatementNode returnState = new ReturnStatementNode();
			returnState.setTest(expression());
			return returnState;
		}
		else if (lookahead.getType() == TokenType.ID) // variable assignment
		{
			if (table.isProcedureName(lookahead.lex) || table.isFunctionName(lookahead.lex))
			{
				procedureStatement();
			}
			else
			{
				// Initial node
				AssignmentStatementNode assign = new AssignmentStatementNode();
				// Create left side
				VariableNode var = variable();
				// Set the variable
				assign.setLvalue(var);
				match(TokenType.ASSIGNOP);
				// Set the value
				ExpressionNode rightSide = expression();
				if (rightSide.getType() == DataType.REAL)
				{
					var.setType(DataType.REAL);
				}
				else
				{
					var.setType(DataType.INTEGER);
				}
				assign.setExpression(rightSide);
				
				return assign;
			}
			
		}
		else if (lookahead.getType() == TokenType.BEGIN) // compound statement
		{
			CompoundStatementNode csn = compoundStatement();
			return csn;
		}
		else
		{
			// Do nothing
			//return null;
		}
		return null;
	}
	
	/**
	 * Rules for variable non-terminal symbol in grammar
	 * @return var A node representing a declared variable 
	 */
	public VariableNode variable()
	{
		String name = lookahead.lex;
		match(TokenType.ID);
		VariableNode var = new VariableNode(name);
		
		if (lookahead.getType() == TokenType.LEFTBRACKET)
		{
			match(TokenType.LEFTBRACKET);
			expression();
			match(TokenType.RIGHTBRACKET);
		}
		else
		{
			// Do nothing
		}
		return var;
	}
	
	/**
	 * Rules for procedure_statement non-terminal symbol in grammar
	 */
	public void procedureStatement()
	{
		match(TokenType.ID);
		if (lookahead.getType() == TokenType.LEFTPARENTHESIS)
		{
			match(TokenType.LEFTPARENTHESIS);
			expressionList();
			match(TokenType.RIGHTPARENTHESIS);
		}
		else
		{
			// Do nothing
		}
	}
	
	/**
	 * Rules for expression_list non-terminal symbol in grammar
	 * @return expList A list of given expressions
	 */
	public ArrayList<ExpressionNode> expressionList()
	{
		ArrayList<ExpressionNode> expList = new ArrayList<ExpressionNode>();
		ExpressionNode exp = expression();
		expList.add(exp);
		
		if (lookahead.getType() == TokenType.COMMA)
		{
			match(TokenType.COMMA);
			expressionList();
		}
		else
		{
			// Do nothing
		}
		return expList;
	}
	
	/**
	 * Rules for expression non-terminal symbol in grammar
	 * @return opNode A node representing a single expression
	 */
	public ExpressionNode expression()
	{
		// Left side expression if option 2, else the only expression
		ExpressionNode exp = simpleExpression();
		
		// If option 2, add a relop and right side expression
		if (isRelop(lookahead))
		{
			// Operation initially has no type, fill it in later
			OperationNode opNode = new OperationNode(null);
			
			if (lookahead.getType() == TokenType.EQUALS)
			{
				opNode.setOperation(lookahead.type);
				opNode.setLeft(exp);
				match(TokenType.EQUALS);
				opNode.setRight(simpleExpression());
			}
			else if (lookahead.getType() == TokenType.NOTEQUAL)
			{
				opNode.setOperation(lookahead.type);
				opNode.setLeft(exp);
				match(TokenType.NOTEQUAL);
				opNode.setRight(simpleExpression());
			}
			else if(lookahead.getType() == TokenType.LESSTHAN)
			{
				opNode.setOperation(lookahead.type);
				opNode.setLeft(exp);
				match(TokenType.LESSTHAN);
				opNode.setRight(simpleExpression());
			}
			else if(lookahead.getType() == TokenType.LESSTHANEQUAL)
			{
				opNode.setOperation(lookahead.type);
				opNode.setLeft(exp);
				match(TokenType.LESSTHANEQUAL);
				opNode.setRight(simpleExpression());
			}
			else if(lookahead.getType() == TokenType.GREATERTHANEQUAL)
			{
				opNode.setOperation(lookahead.type);
				opNode.setLeft(exp);
				match(TokenType.GREATERTHANEQUAL);
				opNode.setRight(simpleExpression());
			}
			else if(lookahead.getType() == TokenType.GREATERTHAN)
			{
				opNode.setOperation(lookahead.type);
				opNode.setLeft(exp);
				match(TokenType.GREATERTHAN);
				opNode.setRight(simpleExpression());
			}
			// Always set left side to be the first expression
			//opNode.setLeft(exp);
			// Return the operation
			return opNode;
		}
		// Case 1, no operation
		else
		{
			// Return the expression
			//return exp;
		}
		return exp;
	}
	
	/**
	 * Rules for simple_expression non-terminal symbol in grammar
	 * @return termNode A node representing a single simple expression
	 */
	public ExpressionNode simpleExpression()
	{
		//OperationNode opNode2 = new OperationNode(null);
		// Check for factor
		if ( (lookahead.getType() == TokenType.ID) ||
			 (lookahead.getType() == TokenType.INTEGER) ||
			 (lookahead.getType() == TokenType.REAL) ||
			 (lookahead.getType() == TokenType.LEFTPARENTHESIS) ||
			 (lookahead.getType() == TokenType.NOT)
			)
		{
			ExpressionNode termNode = term();
			
			return simplePart(termNode);
		}
		else if ( (lookahead.getType() == TokenType.PLUS) ||
				  (lookahead.getType() == TokenType.MINUS)
				)
		{
			sign();
			ExpressionNode termNode = term();
			
			return termNode;
		}
		
		return null;
	}
	
	
	/**
	 * Rules for simple_part non-terminal symbol in grammar
	 * @return addOp A node representing an addop expression
	 */
	public ExpressionNode simplePart(ExpressionNode left)
	{
		if (isAddop(lookahead))
		{
			OperationNode addOp = new OperationNode(lookahead.type);
			
			if (lookahead.getType() == TokenType.PLUS)
			{
				match(TokenType.PLUS);
			}
			else if (lookahead.getType() == TokenType.MINUS)
			{
				match(TokenType.MINUS);
			}
			else if(lookahead.getType() == TokenType.OR)
			{
				match(TokenType.MINUS);
			}
			
			addOp.setLeft(left);
			addOp.setType(left.getType());
			ExpressionNode nextTerm = term();
			addOp.setRight(simplePart(nextTerm));
			//simplePart();
			return addOp;
		}
		else
		{
			// Do nothing
			//return null;
		}
		return left;
	}
	
	/**
	 * Rules for term non-terminal symbol in grammar
	 * @return ExpressionNode A node representing an expression
	 */
	public ExpressionNode term()
	{
		// Call factor
		ExpressionNode factorPart = factor();
		// Get a mulop expression
		return termPart(factorPart);
		
		
		//
	}
	
	/**
	 * Rules for term_part non-terminal symbol in grammar
	 * @return opNode A node representing a mulop expression
	 */
	public ExpressionNode termPart(ExpressionNode left)
	{
		if (isMulop(lookahead))
		{
			OperationNode opNode = new OperationNode(lookahead.type);
			
			if (lookahead.getType() == TokenType.MULTIPLY)
			{
				match(TokenType.MULTIPLY);
			}
			else if (lookahead.getType() == TokenType.DIVIDE)
			{
				match(TokenType.DIVIDE);
			}
			else if (lookahead.getType() == TokenType.DIV)
			{
				match(TokenType.DIV);
			}
			else if (lookahead.getType() == TokenType.MOD)
			{
				match(TokenType.MOD);
			}
			else if (lookahead.getType() == TokenType.AND)
			{
				match(TokenType.AND);
			}
			
			opNode.setLeft(left);
			opNode.setType(left.getType());
			ExpressionNode factorPart = factor();
			opNode.setRight(termPart(factorPart));
			//termPart();
			return opNode;
		}
		else
		{
			// Do nothing
			//return null;
		}
		
		return left;
		
	}
	
	/**
	 * Rules for factor non-terminal symbol in grammar
	 * @return ExpressionNode A node representing a factor
	 */
	public ExpressionNode factor() {
        switch (lookahead.getType()) {
            case LEFTPARENTHESIS:
                match( TokenType.LEFTPARENTHESIS);
                ExpressionNode exp = expression();
                match( TokenType.RIGHTPARENTHESIS);
                return exp;
                //break;
            case INTEGER:
            	// Get value before matching
            	String numInt = lookahead.lex;
            	ValueNode val = new ValueNode(numInt);
            	if (numInt.contains(".") || numInt.contains("e"))
            	{
            		val.setType(DataType.REAL);
            	}
            	else
            	{
            		val.setType(DataType.INTEGER);
            	}
                match(TokenType.INTEGER);
                // Make value node with num
                
                
                // return value node
                return val;
                //break;
            case REAL:
            	// Get value before matching
            	String numReal = lookahead.lex;
            	ValueNode valReal = new ValueNode(numReal);
            	if (numReal.contains(".") || numReal.contains("e"))
            	{
            		valReal.setType(DataType.REAL);
            	}
            	else
            	{
            		valReal.setType(DataType.INTEGER);
            	}
                match(TokenType.REAL);
                // Make value node with num
                
                
                // return value node
                return valReal;
            case NOT:
            	match(TokenType.NOT);
            	ExpressionNode fac = factor();
            	return fac;
                //break;
            case ID:
            	// Get name before matching
            	String name = lookahead.lex;
            	match(TokenType.ID);
            	// Make var node with name
            	VariableNode var = new VariableNode(name);
            	
            	if (lookahead.getType() == TokenType.LEFTBRACKET)
            	{
            		match(TokenType.LEFTBRACKET);
                    expression();
                    match(TokenType.RIGHTBRACKET);
            	}
            	else if (lookahead.getType() == TokenType.LEFTPARENTHESIS)
            	{
            		match(TokenType.LEFTPARENTHESIS);
            		expressionList();
            		match(TokenType.RIGHTPARENTHESIS);
            	}
            	else
            	{
            		// Do nothing
            	}
            	
            	// return variable node
            	return var;
                //break;
            default:
                error("Expected Factor but found type " + lookahead.getType());
                return null;
                //break;
        }
    }
	
	/**
	 * Rules for sign non-terminal symbol in grammar
	 */
	public void sign()
	{
		if (lookahead.getType() == TokenType.PLUS)
		{
			match(TokenType.PLUS);
		}
		else if (lookahead.getType() == TokenType.MINUS)
		{
			match(TokenType.MINUS);
		}
		else
		{
			// do nothing?
		}
	}
	
	
	/**
	 * Determines whether or not a given token is one of the mulop expressions
	 * @param token The input token
	 * @return answer The boolean if is a mulop.
	 */
	public boolean isMulop(Token token) {
        boolean answer = false;
        if( (token.getType() == TokenType.MULTIPLY) || 
            (token.getType() == TokenType.DIVIDE ) ||
        	(token.getType() == TokenType.DIV) ||
        	(token.getType() == TokenType.MOD) ||
        	(token.getType() == TokenType.AND)
          )
        {
            answer = true;
        }
        return answer;
    }
	
	
	/**
	 * Determines whether or not a given token is one of the addop expressions
	 * @param token The input token
	 * @return answer The boolean if is a addop.
	 */
	public boolean isAddop(Token token) {
        boolean answer = false;
        if( (token.getType() == TokenType.PLUS) || 
            (token.getType() == TokenType.MINUS ) ||
        	(token.getType() == TokenType.OR)
          )
        {
            answer = true;
        }
        return answer;
    }
	
	
	/**
	 * Determines whether or not a given token is one of the relop expressions
	 * @param token The input token
	 * @return answer The boolean if is a relop.
	 */
	public boolean isRelop(Token token) {
        boolean answer = false;
        if( (token.getType() == TokenType.EQUALS) || 
            (token.getType() == TokenType.NOTEQUAL ) ||
            (token.getType() == TokenType.LESSTHAN ) ||
        	(token.getType() == TokenType.LESSTHANEQUAL) ||
        	(token.getType() == TokenType.GREATERTHANEQUAL) ||
        	(token.getType() == TokenType.GREATERTHAN)
          )
        {
            answer = true;
        }
        return answer;
    }
	
	
	
	/**
	 * Determines whether or not a given token is one of the statement beginning tokens
	 * @param token The input token
	 * @return answer The boolean if is a statement token.
	 */
	public boolean isStatement(Token token) {
        boolean answer = false;
        if( (token.getType() == TokenType.IF) || 
            (token.getType() == TokenType.WHILE ) ||
        	(token.getType() == TokenType.READ) ||
        	(token.getType() == TokenType.WRITE) ||
        	(token.getType() == TokenType.RETURN) ||
        	(token.getType() == TokenType.BEGIN) ||
        	(token.getType() == TokenType.ID)
          )
        {
            answer = true;
        }
        return answer;
    }
	
	/**
     * Matches the expected token.
     * If the current token in the input stream from the scanner
     * matches the token that is expected, the current token is
     * consumed and the scanner will move on to the next token
     * in the input.
     * The null at the end of the file returned by the
     * scanner is replaced with a fake token containing no
     * type.
     * @param expected The expected token type.
     */
    public void match(TokenType expected) {
        System.out.println("match( " + expected + ")");
        if( this.lookahead.getType() == expected) {
            try {
                this.lookahead = scanner.nextToken();
                if( this.lookahead == null) {
                    this.lookahead = new Token( "End of File", null);
                }
            } catch (IOException ex) {
                error( "Scanner exception");
            }
        }
        else {
            error("Match expected " + expected + " but found " + this.lookahead.getType()
                    + " instead.");
        }
    }
    
    /**
     * Errors out of the parser.
     * Throws a runtime error with message.
     * @param message The error message to print.
     */
    public void error( String message) 
    {
    	System.out.println("Error: " + message);
        throw new RuntimeException(message);
    }

}
