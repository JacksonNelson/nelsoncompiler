package parser;
import java.util.HashMap;
import scanner.TokenType;

/**
 * A symbol table utilizing the HashMap. Allows for adding identifiers of the types
 * variable, function, program, and array.
 * @author Jackson Nelson
 *
 */
public class SymbolTable 
{
	
	//Table is the symbol table - contains the key (name) and value (data structure)
	public HashMap<String, SymbolInfo> table = new HashMap<String, SymbolInfo>();
	
	
	//////////////////////
	//// Adding methods //
	/////////////////////
	
	/**
	 * Adding method for variables
	 * @param name Name of var
	 * @param kind Kind of ID
	 * @param isInteger If var is an integer or real
	 */
	public void addVariable(String name, Boolean isInteger)
	{
		//Boolean isInteger;
		Variable var = new Variable(name, Kind.VARIABLE, isInteger);
		this.addSymbol(var);
	}
	
	
	/**
	 * Adding method for program
	 * @param name Name of program
	 * @param kind Kind of ID
	 */
	public void addProgram(String name)
	{
		Program pro = new Program(name,Kind.PROGRAM);
		this.addSymbol(pro);
	}
	
	
	/**
	 * Adding method for function
	 * @param name Name of function
	 * @param kind Kind of ID
	 */
	public void addFunction(String name)
	{
		Function fun = new Function(name,Kind.FUNCTION);
		this.addSymbol(fun);
	}
	
	
	/**
	 * Adding method for Array
	 * @param name Name of Array
	 * @param kind Kind of ID
	 */
	public void addArray(String name)
	{
		Array arr = new Array(name,Kind.ARRAY);
		this.addSymbol(arr);
	}
	
	/**
	 * Adding method for Array
	 * @param name Name of Array
	 * @param kind Kind of ID
	 */
	public void addProcedure(String name)
	{
		Procedure proc = new Procedure(name,Kind.PROCEDURE);
		this.addSymbol(proc);
	}
	
	
	
	//////////////////////////////
	// Symbol Table put Methods //
	//////////////////////////////
	
	private void addSymbol(Procedure proc) 
	{
		if (containsIdName(proc.name) == false)
		{
			this.table.put(proc.name, proc);
		}		
	}


	/**
	 * Symbol Table put method for Variable
	 * @param var The variable added to the table
	 */
	private void addSymbol(Variable var)
	{
		if (containsIdName(var.name) == false)
		{
			this.table.put(var.name, var);
		}
	}
	
	/**
	 * Symbol Table put method for program
	 * @param pro The Program added to the table
	 */
	private void addSymbol(Program pro)
	{
		//if (containsIdName(pro.name) == false)
		//{
			this.table.put(pro.name, pro);
		//}
	}
	
	
	/**
	 * Symbol table put method for function
	 * @param fun The function added to the table
	 */
	private void addSymbol(Function fun)
	{
		if (containsIdName(fun.name) == false)
		{
			this.table.put(fun.name, fun);
		}
	}
	
	
	/**
	 * Symbol table put method for Array
	 * @param arr The array to be added
	 */
	private void addSymbol(Array arr)
	{
		if (containsIdName(arr.name) == false)
		{
			this.table.put(arr.name, arr);
		}
	}
	
	
	//////////////////////////
	/// Other Methods ////////
	//////////////////////////
	
	/**
	 * Method to check if the symbol table contains an ID
	 * @param name The name to be searching for
	 * @return Boolean if table contains name
	 */
	public boolean containsIdName(String name)
	{
		if (this.table.containsKey(name))
		{
			//System.out.println("ID " + name + " already used");
			return true;
		}
		else
		{
			return false;
		}
	}
	
	/**
	 * Checks if a given entry is a var
	 * @param name
	 * @return
	 */
	public boolean isVariableName(String name)
	{
		// Check that the name exists
		if (this.table.containsKey(name))
		{
			// Check for the correct kind
			if (table.get(name).symbolKind == Kind.VARIABLE)
			{
				return true;
			}
		}
		return false;
	}
	
	/**
	 * Checks if a given entry is a function
	 * @param name
	 * @return
	 */
	public boolean isFunctionName(String name)
	{
		// Check that the name exists
		if (this.table.containsKey(name))
		{
			// Check for the correct kind
			if (table.get(name).symbolKind == Kind.FUNCTION)
			{
				return true;
			}
		}
		return false;
	}
	
	/**
	 * Checks if a given entry is an array
	 * @param name
	 * @return
	 */
	public boolean isArrayName(String name)
	{
		// Check that the name exists
		if (this.table.containsKey(name))
		{
			// Check for the correct kind
			if (table.get(name).symbolKind == Kind.ARRAY)
			{
				return true;
			}
		}
		return false;
	}
	
	/**
	 * Checks if a given entry is a program
	 * @param name
	 * @return
	 */
	public boolean isProgramName(String name)
	{
		// Check that the name exists
		if (this.table.containsKey(name))
		{
			//System.out.println("Contains " + name);
			//System.out.println("test: " + table.get(name).symbolKind);
			// Check for the correct kind
			if (table.get(name).symbolKind == Kind.PROGRAM)
			{
				//System.out.println(name + " is program");
				return true;
			}
		}
		return false;
	}
	
	/**
	 * Checks if a given entry is a program
	 * @param name
	 * @return
	 */
	public boolean isProcedureName(String name)
	{
		// Check that the name exists
		if (this.table.containsKey(name))
		{
			//System.out.println("Contains " + name);
			//System.out.println("test: " + table.get(name).symbolKind);
			// Check for the correct kind
			if (table.get(name).symbolKind == Kind.PROCEDURE)
			{
				//System.out.println(name + " is program");
				return true;
			}
		}
		return false;
	}
	
	/**
	 * String method to neatly print the contents of the symbol table.
	 */
	@Override
	public String toString()
	{
		String top = "Symbol 	Kind\n~~~~~~~~~~~~~~~~~\n";
		String data = "";
		for (String s: table.keySet())
		{
			data = data + s + "\t" + table.get(s).symbolKind + "\n";
		}
		return top + data;
	}
	
	///////////////////////////
	/// Object Subclasses /////
	///////////////////////////
	
	/**
	 * Class containing information about the input symbol. Used as value for HashMap.
	 * @author Jackson Nelson
	 */
	private class SymbolInfo
	{
		String name;
		Kind symbolKind;
		
		// Default constructor
		public SymbolInfo(String name, Kind kind)
		{
			this.name = name;
			this.symbolKind = kind;
		}
		
	}
	
	/**
	 * Subclass Variable extends SymbolInfo, but also can specify if the datatype is an integer
	 * or real number. Used for adding Variable objects
	 * @author Jackson Nelson
	 */
	public class Variable extends SymbolInfo
	{
		boolean isInteger;

		public Variable(String name, Kind kind, Boolean isInteger) 
		{
			super(name, kind);
			this.isInteger = isInteger;
		}
		
	}
	
	/**
	 * Subclass Function extends SymbolInfo, used for adding a function object
	 * @author Jackson Nelson
	 */
	public class Function extends SymbolInfo
	{

		public Function(String name, Kind functionKind) 
		{
			super(name, functionKind);
			
		}
		
	}
	
	/**
	 * Subclass Array extends SymbolInfo, used for adding Array object.
	 * @author Jackson Nelson
	 */
	public class Array extends SymbolInfo
	{

		public Array(String name, Kind arrayKind) 
		{
			super(name, arrayKind);
		}
		
	}
	
	/**
	 * Subclass Program extends SymbolInfo, used for adding Program object.
	 * @author Jackson Nelson
	 *
	 */
	public class Program extends SymbolInfo
	{

		public Program(String name, Kind programKind) 
		{
			super(name, programKind);
		}
		
	}
	
	/**
	 * Subclass Procedure extends SymbolInfo, used for adding procedure object.
	 * @author Jackson Nelson
	 *
	 */
	public class Procedure extends SymbolInfo
	{

		public Procedure(String name, Kind programKind) 
		{
			super(name, programKind);
		}
		
	}
	
	// Default Constructor
	public SymbolTable()
	{
		
	}
}

enum Kind
{
	PROGRAM, VARIABLE, ARRAY, FUNCTION, PROCEDURE
}
