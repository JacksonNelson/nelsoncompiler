/**
 * @author Jackson Nelson
 * Recognizer.java
 * This class acts as the recognizer for a recursive descent parser.
 */
package parser;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringReader;

import scanner.*;

public class Recognizer {
	
	///////////////////////////////
	//       Instance Variables
	///////////////////////////////
	private Token lookahead;
	private MyScanner scanner;
	public SymbolTable table = new SymbolTable();
	
	
	
	///////////////////////////////
	//       Constructors
	///////////////////////////////
	
	public Recognizer( String text, boolean isFilename) 
	{
		if( isFilename) 
		{
			FileInputStream fis = null;
			try 
			{
				fis = new FileInputStream(text);
			} 
			catch (FileNotFoundException ex) 
			{
				error( "No file");
			}
			InputStreamReader isr = new InputStreamReader( fis);
			scanner = new MyScanner( isr);
			
		}
		else 
		{
			scanner = new MyScanner( new StringReader( text));
		}
		try 
		{
			lookahead = scanner.nextToken();
		} 
		catch (IOException ex) 
		{
			error( "Scan error");
		}
		
	}
	
	
	///////////////////////////////
	//       Methods
	///////////////////////////////
	
	/**
	 * Rules for program non-terminal symbol in grammar.
	 */
	public void program()
	{
		match(TokenType.PROGRAM);
		
		String id = lookahead.lex;
		table.addProgram(id);
		
		match(TokenType.ID);
		match(TokenType.SEMICOLON);
		declarations();
		subprogramDeclarations();
		compoundStatement();
		match(TokenType.DOT);
		
	}
	
	/**
	 *  Rules for identifier_list non-terminal symbol in grammar
	 */
	public void identifierList()
	{
		String name = lookahead.lex;
		//System.out.println("Test: " + name);
		table.addVariable(name, false);
		match(TokenType.ID);
		if (lookahead.getType() == TokenType.COMMA)
		{
			match(TokenType.COMMA);
			identifierList();
		}
		else
		{
			// Do nothing
		}
		
	}
	
	/**
	 *  Rules for declarations non-terminal symbol in grammar
	 */
	public void declarations()
	{
		if (lookahead.getType() == TokenType.VAR)
		{
			match(TokenType.VAR);
			identifierList();
			match(TokenType.COLON);
			type();
			match(TokenType.SEMICOLON);
			declarations();
		}
		else
		{
			// Do nothing
		}
	}
	
	/**
	 * Rules for type non-terminal symbol in grammar
	 */
	public void type()
	{
		switch(lookahead.getType())
		{
			case ARRAY:
				match(TokenType.ARRAY);
				match(TokenType.LEFTBRACKET);
				match(TokenType.NUMBER);
				match(TokenType.COLON);
				match(TokenType.NUMBER);
				match(TokenType.RIGHTBRACKET);
				match(TokenType.OF);
				standardType();
				break;
			default:
				standardType();
				break;
		}
		
	}
	
	/**
	 * Rules for standard_type non-terminal symbol in grammar
	 */
	public void standardType()
	{

		if (lookahead.getType() == TokenType.INTEGER)
		{
			match(TokenType.INTEGER);
		}
		else if (lookahead.getType() == TokenType.REAL)
		{
			match(TokenType.REAL);
		}
		
	}
	
	/**
	 * Rules for subprogram_declarations non-terminal symbol in grammar
	 */
	public void subprogramDeclarations()
	{
		
		if ((lookahead.getType() == TokenType.FUNCTION) || (lookahead.getType() == TokenType.PROCEDURE))
		{
			subprogramDeclaration();
			match(TokenType.SEMICOLON);
			subprogramDeclarations();
			
		}
		else
		{
			// Do nothing
		}	
	}
	
	/**
	 * Rules for subprogram_declaration non-terminal symbol in grammar
	 */
	public void subprogramDeclaration()
	{
		subprogramHead();
		declarations();
		compoundStatement();
	}
	
	/**
	 * Rules for subprogram_head non-terminal symbol in grammar
	 */
	public void subprogramHead()
	{
		if(lookahead.getType() == TokenType.FUNCTION)
		{
			match(TokenType.FUNCTION);
			
			String id = lookahead.lex;
			table.addFunction(id);
			
			match(TokenType.ID);
			arguments();
			match(TokenType.COLON);
			standardType();
			match(TokenType.SEMICOLON);
			
			
		}
			
		else if (lookahead.getType() == TokenType.PROCEDURE)
		{
			match(TokenType.PROCEDURE);
			
			String id = lookahead.lex;
			table.addProcedure(id);
			
			match(TokenType.ID);
			arguments();
			match(TokenType.SEMICOLON);
		}
			
		else
		{
			error("Expected Function or Procedure but found "+ lookahead.getType());
		}
			
	}
	
	/**
	 * Rules for arguments non-terminal symbol in grammar
	 */
	public void arguments()
	{
		if (lookahead.getType() == TokenType.LEFTPARENTHESIS)
		{
			match(TokenType.LEFTPARENTHESIS);
			parameterList();
			match(TokenType.RIGHTPARENTHESIS);
		}
		else
		{
			// Do nothing
		}
	}
	
	/**
	 * Rules for parameter_list non-terminal symbol in grammar
	 */
	public void parameterList()
	{
		identifierList();
		match(TokenType.COLON);
		type();
		if (lookahead.getType() == TokenType.SEMICOLON)
		{
			match(TokenType.SEMICOLON);
			parameterList();
		}
		else
		{
			// Do nothing
		}
	}
	
	/**
	 * Rules for compound_statement non-terminal symbol in grammar
	 */
	public void compoundStatement()
	{
		match(TokenType.BEGIN);
		optionalStatements();
		match(TokenType.END);
	}
	
	/**
	 * Rules for optional_statements non-terminal symbol in grammar
	 */
	public void optionalStatements()
	{
		if (isStatement(lookahead))
		{
			statementList();
		}
		else
		{
			// Do nothing
		}
	}
	
	/**
	 * Rules for statement_list non-terminal symbol in grammar
	 */
	public void statementList()
	{
		statement();
		if (lookahead.getType() == TokenType.SEMICOLON)
		{
			match(TokenType.SEMICOLON);
			statementList();
		}
		else
		{
			// Do nothing
		}
	}
	
	/**
	 * Rules for statement non-terminal symbol in grammar
	 */
	public void statement()
	{
		if (lookahead.getType() == TokenType.IF)
		{
			match(TokenType.IF);
			expression();
			match(TokenType.THEN);
			statement();
			match(TokenType.ELSE);
			statement();
		}
		else if (lookahead.getType() == TokenType.WHILE)
		{
			match(TokenType.WHILE);
			expression();
			match(TokenType.DO);
			statement();
		}
		else if (lookahead.getType() == TokenType.READ)
		{
			match(TokenType.READ);
			match(TokenType.LEFTPARENTHESIS);
			match(TokenType.ID);
			match(TokenType.RIGHTPARENTHESIS);
		}
		else if (lookahead.getType() == TokenType.WRITE)
		{
			match(TokenType.WRITE);
			match(TokenType.LEFTPARENTHESIS);
			expression();
			match(TokenType.RIGHTPARENTHESIS);
		}
		else if (lookahead.getType() == TokenType.RETURN)
		{
			match(TokenType.RETURN);
			expression();
		}
		else if (lookahead.getType() == TokenType.ID) // variable
		{
			if (table.isProcedureName(lookahead.lex) || table.isFunctionName(lookahead.lex))
			{
				procedureStatement();
			}
			else
			{
				variable();
				match(TokenType.ASSIGNOP);
				expression();
			}
			
		}
		else if (lookahead.getType() == TokenType.BEGIN) // compound statement
		{
			compoundStatement();
		}
		else
		{
			// Do nothing
		}
	}
	
	/**
	 * Rules for variable non-terminal symbol in grammar
	 */
	public void variable()
	{
		match(TokenType.ID);
		if (lookahead.getType() == TokenType.LEFTBRACKET)
		{
			match(TokenType.LEFTBRACKET);
			expression();
			match(TokenType.RIGHTBRACKET);
		}
		else
		{
			// Do nothing
		}
	}
	
	/**
	 * Rules for procedure_statement non-terminal symbol in grammar
	 */
	public void procedureStatement()
	{
		match(TokenType.ID);
		if (lookahead.getType() == TokenType.LEFTPARENTHESIS)
		{
			match(TokenType.LEFTPARENTHESIS);
			expressionList();
			match(TokenType.RIGHTPARENTHESIS);
		}
		else
		{
			// Do nothing
		}
	}
	
	/**
	 * Rules for expression_list non-terminal symbol in grammar
	 */
	public void expressionList()
	{
		expression();
		if (lookahead.getType() == TokenType.COMMA)
		{
			match(TokenType.COMMA);
			expressionList();
		}
		else
		{
			// Do nothing
		}
	}
	
	/**
	 * Rules for expression non-terminal symbol in grammar
	 */
	public void expression()
	{
		simpleExpression();
		if (isRelop(lookahead))
		{
			if (lookahead.getType() == TokenType.EQUALS)
			{
				match(TokenType.EQUALS);
				simpleExpression();
			}
			else if (lookahead.getType() == TokenType.NOTEQUAL)
			{
				match(TokenType.NOTEQUAL);
				simpleExpression();
			}
			else if(lookahead.getType() == TokenType.LESSTHAN)
			{
				match(TokenType.LESSTHAN);
				simpleExpression();
			}
			else if(lookahead.getType() == TokenType.LESSTHANEQUAL)
			{
				match(TokenType.LESSTHANEQUAL);
				simpleExpression();
			}
			else if(lookahead.getType() == TokenType.GREATERTHANEQUAL)
			{
				match(TokenType.GREATERTHANEQUAL);
				simpleExpression();
			}
			else if(lookahead.getType() == TokenType.GREATERTHAN)
			{
				match(TokenType.GREATERTHAN);
				simpleExpression();
			}
		}
	}
	
	/**
	 * Rules for simple_expression non-terminal symbol in grammar
	 */
	public void simpleExpression()
	{
		// Check for factor
		if ( (lookahead.getType() == TokenType.ID) ||
			 (lookahead.getType() == TokenType.NUMBER) ||
			 (lookahead.getType() == TokenType.LEFTPARENTHESIS) ||
			 (lookahead.getType() == TokenType.NOT)
			)
		{
			term();
			simplePart();
		}
		else if ( (lookahead.getType() == TokenType.PLUS) ||
				  (lookahead.getType() == TokenType.MINUS)
				)
		{
			sign();
			term();
			simplePart();
		}
//		else
//		{
//			// Do nothing
//		}
	}
	
	
	/**
	 * Rules for simple_part non-terminal symbol in grammar
	 */
	public void simplePart()
	{
		if (isAddop(lookahead))
		{
			if (lookahead.getType() == TokenType.PLUS)
			{
				match(TokenType.PLUS);
			}
			else if (lookahead.getType() == TokenType.MINUS)
			{
				match(TokenType.MINUS);
			}
			else if(lookahead.getType() == TokenType.OR)
			{
				match(TokenType.MINUS);
			}
			term();
			simplePart();
		}
		else
		{
			// Do nothing
		}
	}
	
	/**
	 * Rules for term non-terminal symbol in grammar
	 */
	public void term()
	{
		factor();
		termPart();
	}
	
	/**
	 * Rules for term_part non-terminal symbol in grammar
	 */
	public void termPart()
	{
		if (isMulop(lookahead))
		{
			if (lookahead.getType() == TokenType.MULTIPLY)
			{
				match(TokenType.MULTIPLY);
			}
			else if (lookahead.getType() == TokenType.DIVIDE)
			{
				match(TokenType.DIVIDE);
			}
			else if (lookahead.getType() == TokenType.DIV)
			{
				match(TokenType.DIV);
			}
			else if (lookahead.getType() == TokenType.MOD)
			{
				match(TokenType.MOD);
			}
			else if (lookahead.getType() == TokenType.AND)
			{
				match(TokenType.AND);
			}
			
			factor();
			termPart();
			
		}
		else
		{
			// Do nothing
		}
	}
	
	/**
	 * Rules for factor non-terminal symbol in grammar
	 */
	public void factor() {
        switch (lookahead.getType()) {
            case LEFTPARENTHESIS:
                match( TokenType.LEFTPARENTHESIS);
                expression();
                match( TokenType.RIGHTPARENTHESIS);
                break;
            case NUMBER:
                match(TokenType.NUMBER);
                break;
            case NOT:
            	match(TokenType.NOT);
            	factor();
                break;
            case ID:
            	match(TokenType.ID);
            	if (lookahead.getType() == TokenType.LEFTBRACKET)
            	{
            		match(TokenType.LEFTBRACKET);
                    expression();
                    match(TokenType.RIGHTBRACKET);
            	}
            	else if (lookahead.getType() == TokenType.LEFTPARENTHESIS)
            	{
            		match(TokenType.LEFTPARENTHESIS);
            		expressionList();
            		match(TokenType.RIGHTPARENTHESIS);
            	}
            	else
            	{
            		// Do nothing
            	}
                break;
            default:
                error("Expected Factor but found type " + lookahead.getType());
                break;
        }
    }
	
	/**
	 * Rules for sign non-terminal symbol in grammar
	 */
	public void sign()
	{
		if (lookahead.getType() == TokenType.PLUS)
		{
			match(TokenType.PLUS);
		}
		else if (lookahead.getType() == TokenType.MINUS)
		{
			match(TokenType.MINUS);
		}
		else
		{
			// do nothing?
		}
	}
	
	
	/**
	 * Determines whether or not a given token is one of the mulop expressions
	 * @param token The input token
	 * @return answer The boolean if is a mulop.
	 */
	public boolean isMulop(Token token) {
        boolean answer = false;
        if( (token.getType() == TokenType.MULTIPLY) || 
            (token.getType() == TokenType.DIVIDE ) ||
        	(token.getType() == TokenType.DIV) ||
        	(token.getType() == TokenType.MOD) ||
        	(token.getType() == TokenType.AND)
          )
        {
            answer = true;
        }
        return answer;
    }
	
	
	/**
	 * Determines whether or not a given token is one of the addop expressions
	 * @param token The input token
	 * @return answer The boolean if is a addop.
	 */
	public boolean isAddop(Token token) {
        boolean answer = false;
        if( (token.getType() == TokenType.PLUS) || 
            (token.getType() == TokenType.MINUS ) ||
        	(token.getType() == TokenType.OR)
          )
        {
            answer = true;
        }
        return answer;
    }
	
	
	/**
	 * Determines whether or not a given token is one of the relop expressions
	 * @param token The input token
	 * @return answer The boolean if is a relop.
	 */
	public boolean isRelop(Token token) {
        boolean answer = false;
        if( (token.getType() == TokenType.EQUALS) || 
            (token.getType() == TokenType.NOTEQUAL ) ||
            (token.getType() == TokenType.LESSTHAN ) ||
        	(token.getType() == TokenType.LESSTHANEQUAL) ||
        	(token.getType() == TokenType.GREATERTHANEQUAL) ||
        	(token.getType() == TokenType.GREATERTHAN)
          )
        {
            answer = true;
        }
        return answer;
    }
	
	
	
	/**
	 * Determines whether or not a given token is one of the statement beginning tokens
	 * @param token The input token
	 * @return answer The boolean if is a statement token.
	 */
	public boolean isStatement(Token token) {
        boolean answer = false;
        if( (token.getType() == TokenType.IF) || 
            (token.getType() == TokenType.WHILE ) ||
        	(token.getType() == TokenType.READ) ||
        	(token.getType() == TokenType.WRITE) ||
        	(token.getType() == TokenType.RETURN) ||
        	(token.getType() == TokenType.BEGIN) ||
        	(token.getType() == TokenType.ID)
          )
        {
            answer = true;
        }
        return answer;
    }
	
	/**
     * Matches the expected token.
     * If the current token in the input stream from the scanner
     * matches the token that is expected, the current token is
     * consumed and the scanner will move on to the next token
     * in the input.
     * The null at the end of the file returned by the
     * scanner is replaced with a fake token containing no
     * type.
     * @param expected The expected token type.
     */
    public void match(TokenType expected) {
        System.out.println("match( " + expected + ")");
        if( this.lookahead.getType() == expected) {
            try {
                this.lookahead = scanner.nextToken();
                if( this.lookahead == null) {
                    this.lookahead = new Token( "End of File", null);
                }
            } catch (IOException ex) {
                error( "Scanner exception");
            }
        }
        else {
            error("Match expected " + expected + " but found " + this.lookahead.getType()
                    + " instead.");
        }
    }
    
    /**
     * Errors out of the parser.
     * Throws a runtime error with message.
     * @param message The error message to print.
     */
    public void error( String message) 
    {
    	System.out.println("Error: " + message);
        throw new RuntimeException(message);
    }

}
