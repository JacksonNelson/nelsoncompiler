Mini-Pascal Compiler - Jackson Nelson

This is a comprehensive overview of a compiler that will be created for CSC451 during the Spring 2019 semester at Augsburg University. This compiler will function for a Mini-Pascal language, as denoted in the grammar.txt file. This compiler will be written primarily in Java and utilize Java based tools, such as jFlex.

The status of this project is:
	+The scanner of this compiler is tested and implemented, located in the scanner package.
	+The parser consists of a recursive descent recognizer, located in the parser package.
	+Symbol table added to parser package, tested and implemented with recognizer.
	+Recognizer converted to a Parser. Builds a syntax tree from code. Located in Parser package.
	-Additional features will be added in the future.

Author: Jackson Nelson. nelsonj6@augsburg.edu